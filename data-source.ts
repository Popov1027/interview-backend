import * as dotenv from 'dotenv';
import { DataSource } from 'typeorm';
import { Comment } from './src/module/comment/entities/comment.entity';
import { File } from './src/module/file/entities/file.entity';
import { Buffer } from './src/module/file/entities/buffer.entity';
import { Project } from './src/module/project/entities/project.entity';
import { Subactivity } from './src/module/subactivity/entities/subactivity.entity';
import { Task } from './src/module/task/entities/task.entity';
import { UserEntity } from './src/module/user/entities/user.entity';
import { ChangeUserTable1728918820625 } from './src/core/migrations/1728918820625-change-user-table';
import { ChangePhoneNumberAndPasswordUserTable1728983068716 } from './src/core/migrations/1728983068716-change-phone-number-and-password-user-table';

dotenv.config();
export const AppDataSource: DataSource = new DataSource({
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'postgres',
  database: 'postgres',
  schema: 'public',
  logging: false,
  entities: [UserEntity, Comment, File, Buffer, Project, Subactivity, Task],
  migrations: [
    ChangeUserTable1728918820625,
    ChangePhoneNumberAndPasswordUserTable1728983068716,
  ],
  synchronize: false,
  migrationsTableName: 'migrations_info',
  migrationsRun: true,
});

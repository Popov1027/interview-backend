import {
  Controller,
  Get,
  Header,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Query,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApiExcludeController } from '@nestjs/swagger';
import { readFileSync } from 'fs';
import { join } from 'path';

@Controller()
@ApiExcludeController()
export class AppController {
  private readonly _token: string;
  constructor(private readonly _configService: ConfigService) {
    this._token = process.env.SWAGGER_TOKEN;
  }

  /**
   * @description Method used to serve swagger.json file secured by token.
   * @param {string} token Token to access swagger.json
   * @returns {string} Swagger JSON
   */
  @Get('/swagger.json')
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  public getSwaggerJson(@Query('token') token: string): string {
    if (!token || token !== this._token) {
      throw new UnauthorizedException(
        'You are not authorized to access this resource',
      );
    }

    try {
      return readFileSync(join(__dirname, 'swagger.json'), 'utf8').toString();
    } catch (err) {
      throw new NotFoundException('Swagger file not found');
    }
  }
}

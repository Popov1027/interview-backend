import { environment } from './environment';

describe('Environment configuration', () => {
  let originalEnv: NodeJS.ProcessEnv;

  beforeAll(() => {
    originalEnv = process.env;
  });

  beforeEach(() => {
    process.env = { ...originalEnv };
  });

  afterAll(() => {
    process.env = originalEnv;
  });

  it('should return the environment configuration', () => {
    process.env.NODE_ENV = 'test';
    process.env.PG_HOST = 'test_host';
    process.env.PG_DATABASE = 'test_database';
    process.env.PG_SCHEMA = 'test_schema';
    process.env.PG_USER = 'test_user';
    process.env.PG_PASSWORD = 'test_password';
    process.env.PG_PORT = '5432';

    const expectedConfig = {
      env: 'test',
      postgres: {
        host: 'test_host',
        database: 'test_database',
        schema: 'test_schema',
        user: 'test_user',
        password: 'test_password',
        port: 5432,
        logging: true,
      },
    };

    expect(environment()).toEqual(expectedConfig);
  });

  it('should return default values if environment variables are not set', () => {
    const expectedConfig = {
      env: 'test',
      postgres: {
        host: undefined,
        database: undefined,
        schema: undefined,
        user: undefined,
        password: undefined,
        port: NaN,
        logging: true,
      },
    };

    expect(environment()).toEqual(expectedConfig);
  });
});

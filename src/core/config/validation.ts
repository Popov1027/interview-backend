import * as Joi from 'joi';

const logLevelsValidator = Joi.string().custom((value, helpers) => {
  const levels = value.split(',');
  for (const cv of levels) {
    const trimmedValue = cv.trim();
    if (!['log', 'error', 'warn', 'debug', 'verbose'].includes(trimmedValue)) {
      return helpers.error('any.invalid');
    }
  }

  return levels;
}, 'logLevelsValidator');

export const validationSchema = Joi.object({
  NODE_ENV: Joi.string().valid('development').required(),
  LOG_LEVELS: logLevelsValidator
    .allow()
    .default('log,error,warn,debug,verbose'),
  HTTP_PORT: Joi.number().default(3000),

  PG_DATABASE: Joi.string().required(),
  PG_SCHEMA: Joi.string().required(),
  PG_USER: Joi.string().required(),
  PG_PASSWORD: Joi.string().required(),
  PG_PORT: Joi.number().default(5432),

  MINIO_ENDPOINT: Joi.string().required(),
  MINIO_PORT: Joi.number().default(9000),
  MINIO_ACCESSKEY: Joi.string().required(),
  MINIO_SECRETKEY: Joi.string().required(),
  MINIO_USESSL: Joi.boolean().required(),
  MINIO_BUCKET: Joi.string().required(),

  AWS_ACCESS_KEY: Joi.string().required(),
  AWS_SECRET_ACCESS_KEY: Joi.string().required(),
  AWS_S3_REGION: Joi.string().required(),
  AWS_S3_BUCKET_NAME: Joi.string().required(),

  TWILIO_ACCOUNT_SID: Joi.string().required(),
  TWILIO_AUTH_TOKEN: Joi.string().required(),
  TWILIO_PHONE_NUMBER: Joi.string().required(),
});

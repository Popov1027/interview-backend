import { validationSchema } from './validation';

const validate = (data: any) => {
  const { error, value } = validationSchema.validate(data);
  if (error) {
    throw new Error(error.details[0].message);
  }
  return value;
};

describe('Validation Schema', () => {
  test('Valid input should pass validation', () => {
    const validData = {
      NODE_ENV: 'development',
      LOG_LEVELS: 'log,error',
      HTTP_PORT: 3000,
      PG_DATABASE: 'test_db',
      PG_SCHEMA: 'public',
      PG_USER: 'user',
      PG_PASSWORD: 'password',
      PG_PORT: 5432,
      MINIO_ENDPOINT: 'localhost',
      MINIO_PORT: 9000,
      MINIO_ACCESSKEY: 'minio_access_key',
      MINIO_SECRETKEY: 'minio_secret_key',
      MINIO_USESSL: false,
      MINIO_BUCKET: 'bucket',
      AWS_ACCESS_KEY: 'aws_access_key',
      AWS_SECRET_ACCESS_KEY: 'aws_secret_access_key',
      AWS_S3_REGION: 'us-west-1',
      AWS_S3_BUCKET_NAME: 's3_bucket',
      STORAGE_SERVICE: 'service',
      S3: 's3_value',
      MINIO: 'minio_value',
    };

    expect(() => validate(validData)).not.toThrow();
  });

  test('Invalid NODE_ENV should fail validation', () => {
    const invalidData = {
      NODE_ENV: 'production',
      LOG_LEVELS: 'log,error',
      HTTP_PORT: 3000,
      PG_DATABASE: 'test_db',
      PG_SCHEMA: 'public',
      PG_USER: 'user',
      PG_PASSWORD: 'password',
      PG_PORT: 5432,
      MINIO_ENDPOINT: 'localhost',
      MINIO_PORT: 9000,
      MINIO_ACCESSKEY: 'minio_access_key',
      MINIO_SECRETKEY: 'minio_secret_key',
      MINIO_USESSL: false,
      MINIO_BUCKET: 'bucket',
      AWS_ACCESS_KEY: 'aws_access_key',
      AWS_SECRET_ACCESS_KEY: 'aws_secret_access_key',
      AWS_S3_REGION: 'us-west-1',
      AWS_S3_BUCKET_NAME: 's3_bucket',
      STORAGE_SERVICE: 'service',
      S3: 's3_value',
      MINIO: 'minio_value',
    };

    expect(() => validate(invalidData)).toThrow(
      '"NODE_ENV" must be [development]',
    );
  });

  test('Invalid LOG_LEVELS should fail validation', () => {
    const invalidData = {
      NODE_ENV: 'development',
      LOG_LEVELS: 'log,error,invalid_level',
      HTTP_PORT: 3000,
      PG_DATABASE: 'test_db',
      PG_SCHEMA: 'public',
      PG_USER: 'user',
      PG_PASSWORD: 'password',
      PG_PORT: 5432,
      MINIO_ENDPOINT: 'localhost',
      MINIO_PORT: 9000,
      MINIO_ACCESSKEY: 'minio_access_key',
      MINIO_SECRETKEY: 'minio_secret_key',
      MINIO_USESSL: false,
      MINIO_BUCKET: 'bucket',
      AWS_ACCESS_KEY: 'aws_access_key',
      AWS_SECRET_ACCESS_KEY: 'aws_secret_access_key',
      AWS_S3_REGION: 'us-west-1',
      AWS_S3_BUCKET_NAME: 's3_bucket',
      STORAGE_SERVICE: 'service',
      S3: 's3_value',
      MINIO: 'minio_value',
    };

    expect(() => validate(invalidData)).toThrow(
      '"LOG_LEVELS" contains an invalid value',
    );
  });

  test('Missing required field should fail validation', () => {
    const invalidData = {
      NODE_ENV: 'development',
      LOG_LEVELS: 'log,error',
      HTTP_PORT: 3000,
      PG_SCHEMA: 'public',
      PG_USER: 'user',
      PG_PASSWORD: 'password',
      PG_PORT: 5432,
      MINIO_ENDPOINT: 'localhost',
      MINIO_PORT: 9000,
      MINIO_ACCESSKEY: 'minio_access_key',
      MINIO_SECRETKEY: 'minio_secret_key',
      MINIO_USESSL: false,
      MINIO_BUCKET: 'bucket',
      AWS_ACCESS_KEY: 'aws_access_key',
      AWS_SECRET_ACCESS_KEY: 'aws_secret_access_key',
      AWS_S3_REGION: 'us-west-1',
      AWS_S3_BUCKET_NAME: 's3_bucket',
      STORAGE_SERVICE: 'service',
      S3: 's3_value',
      MINIO: 'minio_value',
    };

    expect(() => validate(invalidData)).toThrow('"PG_DATABASE" is required');
  });
});

import { Environment } from '../../common/interfaces/environment';

export const environment = (): Environment => ({
  env: process.env.NODE_ENV,
  postgres: {
    host: process.env.PG_HOST,
    database: process.env.PG_DATABASE,
    schema: process.env.PG_SCHEMA,
    user: process.env.PG_USER,
    password: process.env.PG_PASSWORD,
    port: parseInt(process.env.PG_PORT),
    logging: true,
  },
});

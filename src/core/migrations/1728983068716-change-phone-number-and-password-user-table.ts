import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangePhoneNumberAndPasswordUserTable1728983068716
  implements MigrationInterface
{
  name = 'ChangePhoneNumberAndPasswordUserTable1728983068716';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user" ALTER COLUMN "password" DROP NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user" ALTER COLUMN "password" SET NOT NULL`,
    );
  }
}

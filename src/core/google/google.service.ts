import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { GoogleUserDto } from './dto/google-user.dto';
import { plainToInstance } from 'class-transformer';
import { UserEntity } from '../../module/user/entities/user.entity';
import { AuthService } from '../auth/auth.service';
import { UserDto } from '../../module/user/dto/user.dto';
import { concatMap, from, Observable } from 'rxjs';
import { GoogleStrategyDto } from './dto/google-strategy.dto';

@Injectable()
export class GoogleService {
  private readonly logger: Logger = new Logger(GoogleService.name);

  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    private readonly authService: AuthService,
  ) {}

  validateUser(data: GoogleUserDto): Observable<GoogleStrategyDto> {
    return from(this.userRepository.findOneBy({ email: data.email })).pipe(
      concatMap((existingUser) => {
        if (existingUser) {
          this.logger.log(`User already exists: ${data.email}`);
          return from(
            this.authService.getJWTTokens(existingUser.id, existingUser.email),
          ).pipe(
            concatMap(({ accessToken, refreshToken }) => {
              return from([
                {
                  user: plainToInstance(UserDto, existingUser),
                  accessToken: accessToken,
                  refreshToken: refreshToken,
                },
              ]);
            }),
          );
        } else {
          const newUser = this.userRepository.create(data);
          return from(this.userRepository.save(newUser)).pipe(
            concatMap((user) => {
              if (!user) {
                this.logger.error(
                  `Error in ${this.validateUser.name}: ${JSON.stringify(
                    `Failed to create user`,
                  )}`,
                );
              }

              return from(
                this.authService.getJWTTokens(user.id, user.email),
              ).pipe(
                concatMap(({ accessToken, refreshToken }) => {
                  return from([
                    {
                      user: plainToInstance(UserDto, user),
                      accessToken: accessToken,
                      refreshToken: refreshToken,
                    },
                  ]);
                }),
              );
            }),
          );
        }
      }),
    );
  }
}

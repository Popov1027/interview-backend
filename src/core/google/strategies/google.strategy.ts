import { Injectable, Logger } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, VerifyCallback } from 'passport-google-oauth20';
import { GoogleService } from '../google.service';
import { from, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  private readonly logger: Logger = new Logger(GoogleService.name);

  constructor(private readonly googleService: GoogleService) {
    super({
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_SECRET,
      callbackURL: process.env.GOOGLE_CALLBACK_URL,
      scope: ['profile', 'email'],
    });
  }

  validate(
    accessToken: string,
    refreshToken: string,
    profile: any,
    done: VerifyCallback,
  ) {
    const email = profile.emails?.[0]?.value;
    const firstName = profile.name?.familyName || '';
    const lastName = profile.name?.givenName || '';

    if (!email) {
      this.logger.error(`Error in Google Strategy: Email is missing`);
      return done(null, null);
    }

    this.logger.log(`Validating Google User: ${email}`);

    from(
      this.googleService.validateUser({
        email,
        firstName,
        lastName,
      }),
    )
      .pipe(
        map((user) => {
          this.logger.log(`User validated successfully: ${user.user.email}`);
          return {
            user: user.user,
            accessToken: user.accessToken,
            refreshToken: user.refreshToken,
          };
        }),
        catchError((error) => {
          this.logger.error(`Error in Google Strategy: ${error.message}`);
          return of(null);
        }),
      )
      .subscribe((result) => done(null, result));
  }
}

import { Module } from '@nestjs/common';
import { GoogleStrategy } from './strategies/google.strategy';
import { GoogleService } from './google.service';
import { GoogleController } from './google.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { UserEntity } from '../../module/user/entities/user.entity';
import { SessionSerializer } from '../../common/utils/session-serializer';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserEntity]),
    PassportModule.register({ session: true }),
    AuthModule,
  ],
  controllers: [GoogleController],
  providers: [GoogleService, GoogleStrategy, SessionSerializer],
  exports: [GoogleService, GoogleStrategy, SessionSerializer],
})
export class GoogleModule {}

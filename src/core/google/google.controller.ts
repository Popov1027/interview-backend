import { Controller, Get, Req, Res, UseGuards } from '@nestjs/common';
import { Request, Response } from 'express';
import { GoogleAuthGuard } from './guards/google-auth.guard';
import { ApiTags } from '@nestjs/swagger';
import { GoogleStrategyDto } from './dto/google-strategy.dto';

@ApiTags('Google')
@Controller('auth')
export class GoogleController {
  @Get('google/login')
  @UseGuards(GoogleAuthGuard)
  handleLogin() {
    console.log('enter');
    return { msg: 'Google Authentication' };
  }

  @Get('google/redirect')
  @UseGuards(GoogleAuthGuard)
  handleRedirect(@Req() request: Request, @Res() response: Response) {
    const { accessToken, refreshToken } = request.user as GoogleStrategyDto;
    console.log({ accessToken, refreshToken });
    return response.redirect('https://www.google.com/');
  }

  @Get('status')
  user(@Req() request: Request) {
    if (request.user) {
      return { msg: 'Authenticated' };
    } else {
      return { msg: 'Not Authenticated' };
    }
  }
}

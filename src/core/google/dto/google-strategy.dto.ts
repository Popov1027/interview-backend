import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsObject, IsOptional, IsString } from 'class-validator';
import { UserDto } from '../../../module/user/dto/user.dto';

@Exclude()
export class GoogleStrategyDto {
  @ApiProperty({
    type: UserDto,
  })
  @IsObject()
  @IsNotEmpty()
  @Expose()
  user: UserDto;

  @ApiProperty({
    type: String,
  })
  @IsString()
  @IsOptional()
  @Expose()
  accessToken?: string;

  @ApiProperty({
    type: String,
  })
  @IsString()
  @IsOptional()
  @Expose()
  refreshToken?: string;
}

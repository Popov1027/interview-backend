import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsEmail, IsNotEmpty, IsOptional, IsString } from 'class-validator';

@Exclude()
export class GoogleUserDto {
  @ApiProperty({
    example: 'dev@dreams.dev',
    type: String,
  })
  @IsEmail()
  @IsNotEmpty()
  @Expose()
  email: string;

  @ApiPropertyOptional({
    example: 'Victor',
    type: String,
  })
  @IsOptional()
  @IsString()
  @Expose()
  firstName?: string;

  @ApiPropertyOptional({
    example: 'Creed',
    type: String,
  })
  @IsOptional()
  @IsString()
  @Expose()
  lastName?: string;
}

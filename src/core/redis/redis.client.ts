import { Redis } from 'ioredis';

export class RedisClient {
  private client: Redis;
  private readonly defaultTTL: number;

  constructor(client: Redis, defaultTTL: number) {
    this.client = client;
    this.defaultTTL = defaultTTL;

    this.client.on('error', (error) => {
      console.error('Redis Client Error:', error);
    });

    this.client.on('connect', () => {
      console.log('Redis client connected');
    });

    this.client.on('ready', () => {
      console.log('Redis client ready');
    });

    this.client.on('end', () => {
      console.log('Redis client connection closed');
    });
  }

  async set(key: string, value: any, ttl?: number) {
    const ttlValue = ttl || this.defaultTTL;
    await this.client.set(key, JSON.stringify(value), 'EX', ttlValue);
  }

  async get(key: string) {
    const value = await this.client.get(key);
    return value ? JSON.parse(value) : null;
  }

  async del(key: string) {
    await this.client.del(key);
  }

  async keys(pattern: string): Promise<string[]> {
    return this.client.keys(pattern);
  }
}

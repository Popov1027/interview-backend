import { Inject, Injectable } from '@nestjs/common';
import { RedisClient } from './redis.client';

@Injectable()
export class CacheService {
  constructor(@Inject('REDIS') private readonly redis: RedisClient) {}

  async invalidateCache(pattern: string) {
    const keys = await this.redis.keys(pattern);

    if (keys.length > 0) {
      await Promise.all(keys.map((key) => this.redis.del(key)));
    }
  }
}

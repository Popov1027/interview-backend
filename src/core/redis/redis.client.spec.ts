import { Redis } from 'ioredis';
import { RedisClient } from './redis.client';

type PartialRedisMock = Partial<Record<keyof Redis, jest.Mock>>;

const mockRedisClient: PartialRedisMock = {
  set: jest.fn(),
  get: jest.fn(),
  del: jest.fn(),
  keys: jest.fn(),
  on: jest.fn(),
};

jest.mock('ioredis', () => {
  const actual = jest.requireActual('ioredis');
  return {
    __esModule: true,
    ...actual,
    Redis: jest
      .fn()
      .mockImplementation(() => mockRedisClient as unknown as Redis),
  };
});

describe('RedisClient', () => {
  let redisClient: RedisClient;
  let client: Redis;
  let defaultTTL: number;

  beforeEach(() => {
    redisClient = new RedisClient(client, defaultTTL);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('set', () => {
    it('should set a value with default TTL', async () => {
      await redisClient.set('testKey', { value: 'testValue' });
      expect(mockRedisClient.set).toHaveBeenCalledWith(
        'testKey',
        '{"value":"testValue"}',
        'EX',
        3600,
      );
    });

    it('should set a value with custom TTL', async () => {
      await redisClient.set('testKey', { value: 'testValue' }, 1800);
      expect(mockRedisClient.set).toHaveBeenCalledWith(
        'testKey',
        '{"value":"testValue"}',
        'EX',
        1800,
      );
    });
  });

  describe('get', () => {
    it('should get a value for a key', async () => {
      mockRedisClient.get.mockResolvedValue('{"value":"testValue"}');
      const result = await redisClient.get('testKey');
      expect(result).toEqual({ value: 'testValue' });
      expect(mockRedisClient.get).toHaveBeenCalledWith('testKey');
    });

    it('should return null if key does not exist', async () => {
      mockRedisClient.get.mockResolvedValue(null);
      const result = await redisClient.get('nonExistentKey');
      expect(result).toBeNull();
      expect(mockRedisClient.get).toHaveBeenCalledWith('nonExistentKey');
    });
  });

  describe('del', () => {
    it('should delete a key', async () => {
      await redisClient.del('testKey');
      expect(mockRedisClient.del).toHaveBeenCalledWith('testKey');
    });
  });

  describe('keys', () => {
    it('should return keys matching pattern', async () => {
      mockRedisClient.keys.mockResolvedValue(['key1', 'key2']);
      const result = await redisClient.keys('testPattern:*');
      expect(result).toEqual(['key1', 'key2']);
      expect(mockRedisClient.keys).toHaveBeenCalledWith('testPattern:*');
    });
  });
});

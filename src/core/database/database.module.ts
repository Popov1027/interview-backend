import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostgresConfig } from '../../common/interfaces/postgres-config';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        const databaseConfig = configService.get<PostgresConfig>('postgres');
        return {
          type: 'postgres',
          host: databaseConfig.host,
          database: databaseConfig.database,
          schema: databaseConfig.schema,
          username: databaseConfig.user,
          password: databaseConfig.password,
          port: databaseConfig.port,
          autoLoadEntities: true,
          synchronize: false,
          migrationsRun: true,
          migrationsTableName: 'migrations_info',
          migrationsTransactionMode: 'all',
          logging: true,
          entities: [__dirname + '/../../**/*.entity{.ts,.js}'],
          migrations: [__dirname + '/../migrations/*.{ts,js}'],
        };
      },
    }),
  ],
})
export class DatabaseModule {}

import { Test, TestingModule } from '@nestjs/testing';
import { CacheModule } from '@nestjs/cache-manager';
import { throwError } from 'rxjs';

import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import {
  loginDto,
  mockAuthService,
  registerDto,
  tokenDto,
} from '../../module/user/mock/mock';

describe('AuthController', () => {
  let controller: AuthController;
  let service: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CacheModule.register()],
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: mockAuthService,
        },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('register', () => {
    it('should register user', (done) => {
      controller.register(registerDto).subscribe((result) => {
        expect(result).toEqual(tokenDto);
        expect(service.register).toHaveBeenCalledWith(registerDto);
        done();
      });
    });

    it('should handle error when creating user', (done) => {
      jest
        .spyOn(service, 'register')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to register user')),
        );
      controller.register(registerDto).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to register user');
          done();
        },
      });
    });
  });

  describe('login', () => {
    it('should login user', (done) => {
      controller.login(loginDto).subscribe((result) => {
        expect(result).toEqual(tokenDto);
        expect(service.login).toHaveBeenCalledWith(loginDto);
        done();
      });
    });

    it('should handle error when creating user', (done) => {
      jest
        .spyOn(service, 'login')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to login user')),
        );
      controller.login(loginDto).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to login user');
          done();
        },
      });
    });
  });
});

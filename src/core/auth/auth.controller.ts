import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { TokenDto } from './dto/token.dto';
import { RegisterDto } from './dto/register.dto';
import { from, Observable } from 'rxjs';
import { LoginDto } from './dto/login.dto';
import { AuthService } from './auth.service';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({
    summary: 'Register',
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Registered',
    type: TokenDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to register',
  })
  @HttpCode(HttpStatus.CREATED)
  @Post('register')
  register(@Body() registerDto: RegisterDto): Observable<TokenDto> {
    return from(this.authService.register(registerDto));
  }

  @ApiOperation({
    summary: 'Login',
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Logged in',
    type: TokenDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to login',
  })
  @HttpCode(HttpStatus.CREATED)
  @Post('login')
  login(@Body() loginDto: LoginDto): Observable<TokenDto> {
    return from(this.authService.login(loginDto));
  }
}

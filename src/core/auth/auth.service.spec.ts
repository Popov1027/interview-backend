import { Test, TestingModule } from '@nestjs/testing';
import { HttpException, HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { RegisterDto } from './dto/register.dto';
import { LoginDto } from './dto/login.dto';
import { UserEntity } from '../../module/user/entities/user.entity';
import { AuthService } from './auth.service';

const mockJwtService = {
  signAsync: jest.fn().mockImplementation((payload) => `${payload.sub}_token`),
};

const mockAuthRepository = {
  findOneBy: jest.fn(),
  create: jest.fn(),
  save: jest.fn(),
  merge: jest.fn(),
};

const registerDto: RegisterDto = {
  email: 'test@test.com',
  password: 'password',
  firstName: 'First name',
  lastName: 'Last name',
  phoneNumber: '+37367528138',
};

const loginDto: LoginDto = {
  email: 'test@test.com',
  password: 'password',
};

const mockUserEntity = {
  id: '1',
  email: 'test@test.com',
  password: '$2b$10$examplehash',
};

describe('AuthService', () => {
  let service: AuthService;
  let repository: Repository<UserEntity>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        { provide: JwtService, useValue: mockJwtService },
        {
          provide: getRepositoryToken(UserEntity),
          useValue: mockAuthRepository,
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    repository = module.get<Repository<UserEntity>>(
      getRepositoryToken(UserEntity),
    );
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('register', () => {
    it('should register a new user', (done) => {
      mockAuthRepository.findOneBy.mockResolvedValueOnce(null);
      mockAuthRepository.create.mockReturnValue(mockUserEntity);
      mockAuthRepository.save.mockResolvedValueOnce(mockUserEntity);

      service.register(registerDto).subscribe((result) => {
        expect(result.accessToken).toBeDefined();
        expect(result.refreshToken).toBeDefined();
        expect(mockJwtService.signAsync).toHaveBeenCalledTimes(2);
        expect(mockAuthRepository.findOneBy).toHaveBeenCalledWith({
          email: registerDto.email,
        });
        expect(mockAuthRepository.create).toHaveBeenCalledWith({
          ...registerDto,
          password: expect.any(String),
        });
        expect(mockAuthRepository.save).toHaveBeenCalledWith(mockUserEntity);
        done();
      });
    });

    it('should throw an error if user with same email already exists', (done) => {
      mockAuthRepository.findOneBy.mockResolvedValueOnce(mockUserEntity);

      service.register(registerDto).subscribe({
        error: (error) => {
          expect(error).toBeInstanceOf(HttpException);
          expect(error.message).toBe('User with that email already exists');
          expect(error.getStatus()).toBe(HttpStatus.BAD_REQUEST);
          expect(mockAuthRepository.findOneBy).toHaveBeenCalledWith({
            email: registerDto.email,
          });
          done();
        },
      });
    });

    it('should throw an error if registration fails', (done) => {
      mockAuthRepository.findOneBy.mockRejectedValueOnce(
        new Error('Database error'),
      );

      service.register(registerDto).subscribe({
        error: (error) => {
          expect(error).toBeInstanceOf(HttpException);
          expect(error.message).toBe('Failed to register');
          expect(error.getStatus()).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          expect(mockAuthRepository.findOneBy).toHaveBeenCalledWith({
            email: registerDto.email,
          });
          done();
        },
      });
    });
  });

  describe('login', () => {
    it('should log in a user', (done) => {
      mockAuthRepository.findOneBy.mockResolvedValueOnce(mockUserEntity);

      service.login(loginDto).subscribe((result) => {
        expect(result.accessToken).toBeDefined();
        expect(result.refreshToken).toBeDefined();
        expect(mockJwtService.signAsync).toHaveBeenCalledTimes(2);
        expect(mockAuthRepository.findOneBy).toHaveBeenCalledWith({
          email: loginDto.email,
        });
        done();
      });
    });

    it('should throw an error if user is not found', (done) => {
      mockAuthRepository.findOneBy.mockResolvedValueOnce(null);

      service.login(loginDto).subscribe({
        error: (error: any) => {
          expect(error).toBeInstanceOf(HttpException);
          expect(error.message).toBe('User not found');
          expect(error.getStatus()).toBe(HttpStatus.BAD_REQUEST);
          expect(mockAuthRepository.findOneBy).toHaveBeenCalledWith({
            email: loginDto.email,
          });
          done();
        },
      });
    });

    it('should throw an error if password is incorrect', (done) => {
      mockAuthRepository.findOneBy.mockResolvedValueOnce(mockUserEntity);

      service.login(loginDto).subscribe({
        error: (error: any) => {
          expect(error).toBeInstanceOf(HttpException);
          expect(error.message).toBe('Wrong email or password');
          expect(error.getStatus()).toBe(HttpStatus.BAD_REQUEST);
          expect(mockAuthRepository.findOneBy).toHaveBeenCalledWith({
            email: loginDto.email,
          });
          done();
        },
      });
    });
  });
});

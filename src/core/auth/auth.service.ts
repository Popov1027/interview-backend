import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../../module/user/entities/user.entity';
import { Repository } from 'typeorm';
import { HttpException, HttpStatus, Logger } from '@nestjs/common';
import { RegisterDto } from './dto/register.dto';
import { catchError, concatMap, from, map, Observable, throwError } from 'rxjs';
import * as bcrypt from 'bcrypt';
import { LoginDto } from './dto/login.dto';
import { TokenPayload } from './interface/token-payload.interface';
import { JwtService } from '@nestjs/jwt';
import { TokenDto } from './dto/token.dto';

export class AuthService {
  private readonly logger: Logger = new Logger(AuthService.name);

  constructor(
    @InjectRepository(UserEntity)
    private readonly userService: Repository<UserEntity>,
    private readonly jwtService: JwtService,
  ) {}

  register(
    registerData: RegisterDto,
  ): Observable<{ accessToken: string; refreshToken: string }> {
    return from(this.userService.findOneBy({ email: registerData.email })).pipe(
      concatMap((existingUser) => {
        if (existingUser) {
          this.logger.error(
            `Error in ${this.register.name}: ${JSON.stringify(
              `User with that email already exists`,
            )}`,
          );
          return throwError(
            () =>
              new HttpException(
                'User with that email already exists',
                HttpStatus.BAD_REQUEST,
              ),
          );
        }
        return from(bcrypt.hash(registerData.password, 10)).pipe(
          concatMap((hashedPassword) => {
            const newUser: RegisterDto = {
              ...registerData,
              password: hashedPassword,
            };

            const createUser = this.userService.create(newUser);

            return from(this.userService.save(createUser)).pipe(
              concatMap((createdUser) => {
                delete createdUser.password;
                return this.getJWTTokens(createdUser.id, createdUser.email);
              }),
              catchError(() => {
                this.logger.error(
                  `Error in ${this.register.name}: ${JSON.stringify(
                    `Failed to register`,
                  )}`,
                );
                return throwError(
                  () =>
                    new HttpException(
                      'Failed to register',
                      HttpStatus.INTERNAL_SERVER_ERROR,
                    ),
                );
              }),
            );
          }),
        );
      }),
    );
  }

  login(
    loginData: LoginDto,
  ): Observable<{ accessToken: string; refreshToken: string }> {
    return from(this.userService.findOneBy({ email: loginData.email })).pipe(
      concatMap((user) => {
        if (!user) {
          return throwError(
            () => new HttpException('User not found', HttpStatus.BAD_REQUEST),
          );
        }
        return from(bcrypt.compare(loginData.password, user.password)).pipe(
          concatMap((isPasswordMatching: boolean) => {
            if (!isPasswordMatching) {
              this.logger.error(
                `Error in ${this.register.name}: ${JSON.stringify(
                  `Wrong email or password`,
                )}`,
              );
              return throwError(
                () =>
                  new HttpException(
                    'Wrong email or password',
                    HttpStatus.BAD_REQUEST,
                  ),
              );
            }
            return this.getJWTTokens(user.id, user.email);
          }),
        );
      }),
    );
  }

  getJWTTokens(userId: string, email: string): Observable<TokenDto> {
    const payload: TokenPayload = {
      sub: userId,
      email: email,
    };

    const accessToken$ = from(
      this.jwtService.signAsync(payload, {
        secret: process.env.JWT_ACCESS_TOKEN_SECRET,
        expiresIn: process.env.JWT_ACCESS_TOKEN_EXPIRATION_TIME,
      }),
    );

    const refreshToken$ = from(
      this.jwtService.signAsync(payload, {
        secret: process.env.JWT_REFRESH_TOKEN_SECRET,
        expiresIn: process.env.JWT_REFRESH_TOKEN_EXPIRATION_TIME,
      }),
    );

    return accessToken$.pipe(
      concatMap((accessToken) =>
        refreshToken$.pipe(
          map((refreshToken) => ({
            accessToken,
            refreshToken,
          })),
        ),
      ),
    );
  }
}

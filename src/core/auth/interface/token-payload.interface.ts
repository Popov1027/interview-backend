export interface TokenPayload {
  sub: string;
  email: string;
}

export interface UserDecorator {
  userId: string;
  email: string;
}

import { Test, TestingModule } from '@nestjs/testing';
import { JwtStrategy } from './jwt.strategy';
import { ExtractJwt } from 'passport-jwt';
import * as dotenv from 'dotenv';
import { UserDecorator } from '../interface/token-payload.interface';

dotenv.config();

describe('JwtStrategy', () => {
  let strategy: JwtStrategy;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [JwtStrategy],
    }).compile();

    strategy = module.get<JwtStrategy>(JwtStrategy);
  });

  it('should be defined', () => {
    expect(strategy).toBeDefined();
  });

  it('should validate the payload', async () => {
    const payload: UserDecorator = {
      userId: undefined,
      email: 'user@example.com',
    };
    const result = await strategy.validate(payload);
    expect(result).toEqual(payload);
  });

  it('should throw an error if JWT is not valid', async () => {
    const invalidPayload = { sub: null, email: null };
    try {
      await strategy.validate(invalidPayload);
    } catch (error) {
      expect(error).toBeDefined();
    }
  });

  it('should extract JWT from the header as a Bearer token', () => {
    const req = {
      headers: {
        authorization: 'Bearer myJwtToken',
      },
    };
    const jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    expect(jwtFromRequest(req)).toBe('myJwtToken');
  });

  it('should use the correct secret key from environment variables', () => {
    const secretKey = process.env.JWT_ACCESS_TOKEN_SECRET;
    expect(secretKey).toBeDefined();
  });

  it('should have ignoreExpiration set to false', () => {
    const strategyInstance = new JwtStrategy();
    expect(strategyInstance).toBeDefined();
  });
});

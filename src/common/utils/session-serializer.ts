import { Injectable, Logger } from '@nestjs/common';
import { PassportSerializer } from '@nestjs/passport';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserDto } from '../../module/user/dto/user.dto';
import { UserEntity } from '../../module/user/entities/user.entity';
import { from, map, catchError } from 'rxjs';

@Injectable()
export class SessionSerializer extends PassportSerializer {
  private readonly logger: Logger = new Logger(SessionSerializer.name);

  constructor(
    @InjectRepository(UserEntity)
    readonly usersRepository: Repository<UserEntity>,
  ) {
    super();
  }

  serializeUser(user: UserDto, done: Function) {
    console.log('ENTER');

    this.logger.log(`Serializing user: ${JSON.stringify(user)}`);
    done(null, user);
  }

  deserializeUser(payload: any, done: Function) {
    console.log('ENTER');
    this.logger.log(`Deserialize user: ${JSON.stringify(payload)}`);

    from(this.usersRepository.findOneBy({ id: payload.id })).pipe(
      map((user: UserEntity | null) => {
        if (!user) {
          return done(null, null);
        }
        return done(null, user);
      }),
      catchError((error) => {
        this.logger.error(`Error during deserialization: ${error.message}`);
        return from([done(error, null)]);
      }),
    );
  }
}

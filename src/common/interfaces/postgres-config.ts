import { LoggerOptions } from 'typeorm';

export interface PostgresConfig {
  host: string;
  database: string;
  schema: string;
  user: string;
  password: string;
  port: number;
  logging: LoggerOptions;
}

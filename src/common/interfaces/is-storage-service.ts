import { Observable } from 'rxjs';

export interface IStorageService {
  upload(file: Express.Multer.File): Observable<{ url: string }>;
  deleteFile(filename: string): Observable<void>;
}

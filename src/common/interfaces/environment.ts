import { PostgresConfig } from './postgres-config';

export interface Environment {
  env: string;
  postgres: PostgresConfig;
}

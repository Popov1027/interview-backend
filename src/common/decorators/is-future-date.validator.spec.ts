import { validate, ValidationError } from 'class-validator';
import {
  IsFutureDate,
  IsFutureDateConstraint,
} from './is-future-date.validator';

describe('IsFutureDateConstraint', () => {
  let constraint: IsFutureDateConstraint;

  beforeEach(() => {
    constraint = new IsFutureDateConstraint();
  });

  it('should return true if the date is in the future', () => {
    const futureDate = '31/12/2099';
    expect(constraint.validate(futureDate)).toBe(true);
  });

  it('should return false if the date is in the past', () => {
    const pastDate = '01/01/2000';
    expect(constraint.validate(pastDate)).toBe(false);
  });

  it('should return false if the date is today', () => {
    const today = new Date().toLocaleDateString('en-GB').replace(/\//g, '/'); // e.g., '17/07/2024'
    expect(constraint.validate(today)).toBe(false);
  });

  it('should return the correct default message', () => {
    expect(constraint.defaultMessage()).toBe('Deadline must be a future date');
  });
});

describe('IsFutureDate Decorator', () => {
  class TestClass {
    @IsFutureDate()
    deadline: string;
  }

  it('should pass validation if the date is in the future', async () => {
    const testInstance = new TestClass();
    testInstance.deadline = '31/12/2099';
    const errors: ValidationError[] = await validate(testInstance);
    expect(errors.length).toBe(0);
  });

  it('should fail validation if the date is in the past', async () => {
    const testInstance = new TestClass();
    testInstance.deadline = '01/01/2000';
    const errors: ValidationError[] = await validate(testInstance);
    expect(errors.length).toBeGreaterThan(0);
    expect(errors[0].constraints).toHaveProperty(
      'isFutureDate',
      'Deadline must be a future date',
    );
  });

  it('should fail validation if the date is today', async () => {
    const testInstance = new TestClass();
    testInstance.deadline = new Date()
      .toLocaleDateString('en-GB')
      .replace(/\//g, '/'); // e.g., '17/07/2024'
    const errors: ValidationError[] = await validate(testInstance);
    expect(errors.length).toBeGreaterThan(0);
    expect(errors[0].constraints).toHaveProperty(
      'isFutureDate',
      'Deadline must be a future date',
    );
  });
});

import { Logger, RequestMethod, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { writeFileSync } from 'fs';
import { join } from 'path';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './core/exception/http-exception-filter';
import * as session from 'express-session';
import * as passport from 'passport';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = new ConfigService();
  const port = await configService.get('HTTP_PORT');

  app.useGlobalFilters(new HttpExceptionFilter());

  app.use(
    session({
      secret: process.env.GOOGLE_SESSION_SECRET,
    }),
  );

  app.use(passport.initialize());
  app.use(passport.session());

  app.setGlobalPrefix('api', {
    exclude: [
      {
        path: '/swagger.json',
        method: RequestMethod.GET,
      },
    ],
  });

  const config = new DocumentBuilder()
    .setTitle('Inuarashi')
    .setDescription('The Inuarashi application')
    .setVersion('1.0')
    .addBearerAuth({ type: 'http', name: 'AccessToken' })
    .build();

  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.enableCors({
    origin: '*',
  });
  const document = SwaggerModule.createDocument(app, config);
  writeFileSync(
    join(__dirname, 'swagger.json'),
    JSON.stringify(document, null, 2),
  );
  SwaggerModule.setup('api', app, document);

  await app.listen(port).then(() => {
    Logger.log(`Application is running  on: http://localhost:${port}`);
    Logger.log(`Find docs on http://localhost:${port}/api`);
  });
}
bootstrap().catch((error) => {
  Logger.error(error, 'NestFactory');
  process.exit(1);
});

import { ConfigModule, ConfigService } from '@nestjs/config';

import { Module, ValidationPipe } from '@nestjs/common';
import { APP_PIPE } from '@nestjs/core';
import { environment } from './core/config/environment';
import { DatabaseModule } from './core/database/database.module';
import { validationSchema } from './core/config/validation';
import { MulterModule } from '@nestjs/platform-express';
import { Modules } from './module';
import { RedisModule } from './core/redis/redis.module';
import { AppController } from './app.controller';

@Module({
  imports: [
    RedisModule,
    ConfigModule.forRoot({
      isGlobal: true,
      load: [environment],
      validationSchema,
    }),
    MulterModule.register({
      dest: './uploads',
    }),
    DatabaseModule,
    ...Modules,
  ],
  providers: [
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
    ConfigService,
  ],
  controllers: [AppController],
})
export class AppModule {}

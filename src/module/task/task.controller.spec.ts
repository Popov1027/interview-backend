import { Test, TestingModule } from '@nestjs/testing';
import { TaskController } from './task.controller';
import { TaskService } from './task.service';
import { CacheModule } from '@nestjs/cache-manager';
import { throwError } from 'rxjs';
import {
  createTaskDto,
  mockTask,
  mockTaskService,
  queryDto,
  taskId,
  updateTaskDto,
  user,
} from './mock/mock';

describe('TaskController', () => {
  let controller: TaskController;
  let service: TaskService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CacheModule.register()],
      controllers: [TaskController],
      providers: [
        {
          provide: TaskService,
          useValue: mockTaskService,
        },
      ],
    }).compile();

    controller = module.get<TaskController>(TaskController);
    service = module.get<TaskService>(TaskService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('create', () => {
    it('should create a subactivity', (done) => {
      controller.create(createTaskDto).subscribe((result) => {
        expect(result).toEqual(mockTask);
        expect(service.create).toHaveBeenCalledWith(createTaskDto);
        done();
      });
    });

    it('should handle error when creating subactivity', (done) => {
      jest
        .spyOn(service, 'create')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to create subactivity')),
        );
      controller.create(createTaskDto).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to create subactivity');
          done();
        },
      });
    });
  });

  describe('userAssignment', () => {
    it('should assign task to user', (done) => {
      controller.userAssignment(user, taskId).subscribe((result) => {
        expect(result).toEqual(mockTask);
        expect(service.userAssignment).toHaveBeenCalledWith(user, taskId);
        done();
      });
    });

    it('should handle error when assign task to user', (done) => {
      jest
        .spyOn(service, 'create')
        .mockReturnValueOnce(
          throwError(() => new Error('Assign task to user')),
        );
      controller.userAssignment(user, taskId).subscribe({
        error: (error) => {
          expect(error.message).toBe('Assign task to user');
          done();
        },
      });
    });
  });

  describe('findAll', () => {
    it('should find all tasks', (done) => {
      controller.findAll(queryDto).subscribe((result) => {
        expect(result).toEqual([mockTask]);
        expect(service.findAll).toHaveBeenCalledWith(queryDto);
        done();
      });
    });

    it('should handle error when finding all tasks', (done) => {
      jest
        .spyOn(service, 'findAll')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to get all tasks')),
        );

      controller.findAll(queryDto).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to get all tasks');
          done();
        },
      });
    });
  });

  describe('findOne', () => {
    it('should find one task by id', (done) => {
      controller.findOne(taskId).subscribe((result) => {
        expect(result).toEqual(mockTask);
        expect(service.findOne).toHaveBeenCalledWith(taskId);
        done();
      });
    });

    it('should handle error when finding task by id', (done) => {
      jest
        .spyOn(service, 'findOne')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to get task by id')),
        );
      controller.findOne(taskId).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to get task by id');
          done();
        },
      });
    });
  });

  describe('update', () => {
    it('should update a task', (done) => {
      controller.update(taskId, updateTaskDto).subscribe((result) => {
        expect(result).toEqual(mockTask);
        expect(service.update).toHaveBeenCalledWith(taskId, updateTaskDto);
        done();
      });
    });

    it('should handle error when updating task', (done) => {
      jest
        .spyOn(service, 'update')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to update task')),
        );
      controller.update(taskId, updateTaskDto).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to update task');
          done();
        },
      });
    });
  });

  describe('remove', () => {
    it('should remove a task', (done) => {
      controller.remove(taskId).subscribe((result) => {
        expect(result).toBeUndefined();
        expect(service.remove).toHaveBeenCalledWith(taskId);
        done();
      });
    });

    it('should handle error when removing task', (done) => {
      jest
        .spyOn(service, 'remove')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to delete task')),
        );
      controller.remove(taskId).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to delete task');
          done();
        },
      });
    });
  });
});

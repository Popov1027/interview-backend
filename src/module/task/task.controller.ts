import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  UseGuards,
  Query,
} from '@nestjs/common';
import { TaskService } from './task.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { TaskDto } from './dto/task.dto';
import { from, Observable } from 'rxjs';
import { TaskQueryDto } from './dto/task.query.dto';
import { JwtAuthGuard } from '../../core/auth/guards/auth.guard';
import { UserDecorator } from '../../core/auth/interface/token-payload.interface';
import { User } from '../user/dto/user.decorator';

@ApiTags('Task')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('task')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @ApiOperation({ summary: 'Endpoint to create task' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Task was created',
    type: TaskDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to create task',
  })
  @Post()
  create(@Body() createTaskDto: CreateTaskDto): Observable<TaskDto> {
    return from(this.taskService.create(createTaskDto));
  }

  @ApiOperation({ summary: 'Endpoint for assigning the task to the user' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Task was assigned',
    type: TaskDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to assigned task',
  })
  @Post('/assign-user/:taskId')
  userAssignment(
    @User() user: UserDecorator,
    @Param('taskId') taskId: string,
  ): Observable<TaskDto> {
    return from(this.taskService.userAssignment(user, taskId));
  }

  @ApiOperation({ summary: 'Endpoint to get all tasks' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get all tasks',
    type: [TaskDto],
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to get all tasks',
  })
  @Get()
  findAll(@Query() taskQueryDto: TaskQueryDto): Observable<TaskDto[]> {
    return from(this.taskService.findAll(taskQueryDto));
  }

  @ApiOperation({ summary: 'Endpoint to get task by id' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Get task by id',
    type: TaskDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to get task by id',
  })
  @Get(':id')
  findOne(@Param('id') id: string): Observable<TaskDto> {
    return from(this.taskService.findOne(id));
  }

  @ApiOperation({ summary: 'Endpoint to update task' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Update task',
    type: TaskDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to update task',
  })
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateTaskDto: UpdateTaskDto,
  ): Observable<TaskDto> {
    return from(this.taskService.update(id, updateTaskDto));
  }

  @ApiOperation({ summary: 'Endpoint to delete task' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Delete task',
    type: TaskDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to delete task',
  })
  @Delete(':id')
  remove(@Param('id') id: string): Observable<void> {
    return from(this.taskService.remove(id));
  }
}

import { Module } from '@nestjs/common';
import { TaskService } from './task.service';
import { TaskController } from './task.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Task } from './entities/task.entity';
import { TaskGateway } from './gateway/task.gateway';
import { TwilioModule } from 'nestjs-twilio';
import { Buffer } from '../file/entities/buffer.entity';
import { CacheService } from '../../core/redis/cache.service';
import { UserEntity } from '../user/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Task, UserEntity, Buffer]),
    TwilioModule.forRoot({
      isGlobal: true,
      accountSid: process.env.TWILIO_ACCOUNT_SID,
      authToken: process.env.TWILIO_AUTH_TOKEN,
    }),
  ],
  controllers: [TaskController],
  providers: [TaskService, TaskGateway, CacheService],
})
export class TaskModule {}

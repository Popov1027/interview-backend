import { CreateTaskDto } from '../dto/create-task.dto';
import {
  Sort,
  TaskPriority,
  TaskStatus,
} from '../../../common/enums/task.enum';
import { of } from 'rxjs';
import { UserDecorator } from '../../../core/auth/interface/token-payload.interface';

const createTaskDto: CreateTaskDto = {
  name: 'Test Task',
  description: 'Test Description',
  deadline: '2025-01-22T13:55:37.839Z',
  priority: TaskPriority.MEDIUM,
  status: TaskStatus.TODO,
  projectId: '1',
};

const updateTaskDto = { ...createTaskDto };

const taskId = '1';
const user: UserDecorator = {
  userId: '1',
  email: 'test@mail.com',
};

const mockTask = {
  id: '1',
  name: 'Test Task',
  description: 'Test Description',
  deadline: '2025-01-22T13:55:37.839Z',
  priority: 'medium',
  status: 'todo',
  projectId: '1',
  project: {},
  userId: '1',
  user: {},
  createdAt: new Date(),
  updatedAt: new Date(),
};

const queryDto = {
  priority: TaskPriority.LOW,
  status: TaskStatus.TODO,
  sort: Sort.ASC,
  name: 'Test',
  projectId: '1',
};

const mockTaskService = {
  create: jest.fn().mockReturnValue(of(mockTask)),
  userAssignment: jest.fn().mockReturnValue(of(mockTask)),
  findAll: jest.fn().mockReturnValue(of([mockTask])),
  findOne: jest.fn().mockReturnValue(of(mockTask)),
  update: jest.fn().mockReturnValue(of(mockTask)),
  remove: jest.fn().mockReturnValue(of(undefined)),
};

export {
  createTaskDto,
  updateTaskDto,
  mockTaskService,
  taskId,
  user,
  queryDto,
  mockTask,
};

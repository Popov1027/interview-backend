import { Test, TestingModule } from '@nestjs/testing';
import { TaskService } from './task.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Task } from './entities/task.entity';
import { CacheService } from '../../core/redis/cache.service';
import { Repository } from 'typeorm';
import { HttpStatus } from '@nestjs/common';
import { Sort, TaskPriority, TaskStatus } from '../../common/enums/task.enum';
import { UserDecorator } from '../auth/interface/token-payload.interface';
import { Auth } from '../auth/entities/user.entity';
import { TaskGateway } from './gateway/task.gateway';
import { TwilioService } from 'nestjs-twilio';
import { TaskQueryDto } from './dto/task.query.dto';
import { TaskDto } from './dto/task.dto';

const user: UserDecorator = { userId: '1', email: 'testuser' };
const taskId = '1';

const createTaskDto = {
  name: 'Test Task',
  description: 'Test Description',
  deadline: '11/11/2025',
  projectId: '1',
  priority: TaskPriority.LOW,
  status: TaskStatus.TODO,
};

const updateTaskDto = { ...createTaskDto };

const mockTask: TaskDto = {
  id: '1',
  name: 'Test Task',
  description: 'Test Description',
  deadline: '11/11/2025',
  projectId: '1',
  priority: TaskPriority.LOW,
  status: TaskStatus.TODO,
  createdAt: new Date(),
  updatedAt: new Date(),
  userId: '1',
  user: undefined,
  project: undefined,
  subactivities: [],
  comments: [],
  files: [],
};

const mockTaskRepository = {
  create: jest.fn().mockReturnValue(mockTask),
  save: jest.fn().mockResolvedValue(mockTask),
  findOneBy: jest.fn().mockResolvedValue(mockTask),
  merge: jest.fn().mockResolvedValue(mockTask),
  find: jest.fn().mockResolvedValue([mockTask]),
  remove: jest.fn().mockResolvedValue(undefined),
};

const mockAuthRepository = {
  findOneBy: jest.fn().mockResolvedValue({ id: '1', userId: '1' }),
};

const mockRedisClient = {
  get: jest.fn().mockResolvedValue(null),
  set: jest.fn().mockResolvedValue(null),
  del: jest.fn().mockResolvedValue(null),
};

const mockTaskGateway = {
  emitTaskCreated: jest.fn().mockResolvedValue(null),
  emitAssignedTask: jest.fn().mockResolvedValue(null),
  emitTaskUpdated: jest.fn().mockResolvedValue(null),
};

const mockTwilioService = {
  client: {
    messages: {
      create: jest.fn().mockResolvedValue(null),
    },
  },
};

const mockCacheService = {
  invalidateCache: jest.fn().mockResolvedValue(null),
};

describe('TaskService', () => {
  let service: TaskService;
  let taskRepository: Repository<Task>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TaskService,
        {
          provide: getRepositoryToken(Task),
          useValue: mockTaskRepository,
        },
        {
          provide: getRepositoryToken(Auth),
          useValue: mockAuthRepository,
        },
        {
          provide: 'REDIS',
          useValue: mockRedisClient,
        },
        {
          provide: TaskGateway,
          useValue: mockTaskGateway,
        },
        {
          provide: TwilioService,
          useValue: mockTwilioService,
        },
        {
          provide: CacheService,
          useValue: mockCacheService,
        },
      ],
    }).compile();

    service = module.get<TaskService>(TaskService);
    taskRepository = module.get<Repository<Task>>(getRepositoryToken(Task));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('should create a task', (done) => {
      service.create(createTaskDto).subscribe((result) => {
        expect(result).toEqual(mockTask);
        expect(mockTaskGateway.emitTaskCreated).toHaveBeenCalledWith(
          expect.objectContaining({ name: 'Test Task' }),
        );
        expect(mockCacheService.invalidateCache).toHaveBeenCalledWith('task_*');
        done();
      });
    });

    it('should throw an error if creation fails', (done) => {
      jest
        .spyOn(taskRepository, 'save')
        .mockRejectedValueOnce(new Error('Error creating task'));
      service.create({ ...createTaskDto }).subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to create task: Error: Error creating task',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });

  describe('userAssignment', () => {
    it('should assign a user to a task', (done) => {
      service.userAssignment(user, taskId).subscribe((result) => {
        expect(result).toEqual(mockTask);
        expect(mockRedisClient.del).toHaveBeenCalledWith('task_1');
        done();
      });
    });

    it('should unassign a user from a task if already assigned', (done) => {
      jest.spyOn(taskRepository, 'findOneBy').mockResolvedValueOnce(mockTask);

      service.userAssignment(user, taskId).subscribe((result) => {
        expect(result).toEqual(mockTask);
        expect(mockTaskGateway.emitAssignedTask).toHaveBeenCalledWith(null);
        expect(mockRedisClient.del).toHaveBeenCalledWith('task_1');
        done();
      });
    });

    it('should throw an error if user not found', (done) => {
      jest.spyOn(mockAuthRepository, 'findOneBy').mockResolvedValueOnce(null);
      service.userAssignment(user, taskId).subscribe({
        error: (error) => {
          expect(error.response).toBe('User not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if task not found', (done) => {
      jest.spyOn(taskRepository, 'findOneBy').mockResolvedValueOnce(null);
      service.userAssignment(user, taskId).subscribe({
        error: (error) => {
          expect(error.response).toBe('Task not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if assignment fails', (done) => {
      jest.spyOn(taskRepository, 'findOneBy').mockResolvedValueOnce(mockTask);
      jest
        .spyOn(taskRepository, 'save')
        .mockRejectedValueOnce(new Error('Error assigning task'));

      service.userAssignment(user, taskId).subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to assignment task: Error: Error assigning task',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });

  describe('findAll', () => {
    const taskQueryDto: TaskQueryDto = {
      name: 'Test',
      projectId: '1',
      priority: TaskPriority.LOW,
      status: TaskStatus.TODO,
      sort: Sort.ASC,
    };

    it('should find all tasks', (done) => {
      service.findAll(taskQueryDto).subscribe((result) => {
        expect(result).toEqual([mockTask]);
        done();
      });
    });

    it('should return cached tasks', (done) => {
      jest.spyOn(mockRedisClient, 'get').mockResolvedValueOnce([mockTask]);
      service.findAll(taskQueryDto).subscribe((result) => {
        expect(result).toEqual([mockTask]);
        done();
      });
    });
  });

  describe('findOne', () => {
    it('should find one task', (done) => {
      service.findOne('1').subscribe((result) => {
        expect(result).toEqual(mockTask);
        done();
      });
    });

    it('should return cached task', (done) => {
      jest.spyOn(mockRedisClient, 'get').mockResolvedValueOnce(mockTask);
      service.findOne('1').subscribe((result) => {
        expect(result).toEqual(mockTask);
        done();
      });
    });

    it('should throw an error if task not found', (done) => {
      jest.spyOn(taskRepository, 'findOneBy').mockResolvedValueOnce(null);
      service.findOne('1').subscribe({
        error: (error) => {
          expect(error.response).toBe('Task with ID 1 not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });
  });

  describe('update', () => {
    it('should update a task', (done) => {
      service.update('1', updateTaskDto).subscribe((result) => {
        expect(result).toEqual(mockTask);
        expect(mockRedisClient.set).toHaveBeenCalledWith(
          'task_1',
          expect.anything(),
        );
        expect(mockCacheService.invalidateCache).toHaveBeenCalledWith('task_*');
        expect(mockTaskGateway.emitTaskUpdated).toHaveBeenCalledWith(
          expect.objectContaining({ name: 'Test Task' }),
        );
        done();
      });
    });

    it('should throw an error if task not found', (done) => {
      jest.spyOn(taskRepository, 'findOneBy').mockResolvedValueOnce(null);
      service.update('1', updateTaskDto).subscribe({
        error: (error) => {
          expect(error.response).toBe('Task not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if failed to update task', (done) => {
      jest.spyOn(taskRepository, 'findOneBy').mockResolvedValueOnce(mockTask);
      jest
        .spyOn(taskRepository, 'save')
        .mockRejectedValueOnce(new Error('Error updating task'));

      service.update('1', updateTaskDto).subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to update task: Error: Error updating task',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });

  describe('remove', () => {
    it('should remove a task', (done) => {
      service.remove('1').subscribe(() => {
        expect(mockRedisClient.del).toHaveBeenCalledWith('task_1');
        expect(mockCacheService.invalidateCache).toHaveBeenCalledWith('task_*');
        done();
      });
    });

    it('should throw an error if task not found', (done) => {
      jest.spyOn(taskRepository, 'findOneBy').mockResolvedValueOnce(null);
      service.remove('1').subscribe({
        error: (error) => {
          expect(error.response).toBe('Task not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if failed to remove task', (done) => {
      jest.spyOn(taskRepository, 'findOneBy').mockResolvedValueOnce(mockTask);
      jest
        .spyOn(taskRepository, 'remove')
        .mockRejectedValueOnce(new Error('Error removing task'));
      service.remove('1').subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to remove task: Error: Error removing task',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });
});

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  Index,
  OneToMany,
} from 'typeorm';
import { Project } from '../../project/entities/project.entity';
import { TaskPriority, TaskStatus } from '../../../common/enums/task.enum';
import { Subactivity } from '../../subactivity/entities/subactivity.entity';
import { Comment } from '../../comment/entities/comment.entity';
import { File } from '../../file/entities/file.entity';
import { UserEntity } from '../../user/entities/user.entity';

@Entity()
export class Task {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @Column()
  name: string;

  @Column({ nullable: true })
  description: string;

  @Column({ nullable: true })
  deadline: string;

  @Column({
    type: 'enum',
    enum: TaskPriority,
    default: TaskPriority.MEDIUM,
  })
  priority: TaskPriority;

  @Column({
    type: 'enum',
    enum: TaskStatus,
    default: TaskStatus.TODO,
  })
  status: TaskStatus;

  @OneToMany(() => Subactivity, (subactivity) => subactivity.task)
  subactivities!: Subactivity[];

  @OneToMany(() => Comment, (comment) => comment.task)
  comments!: Comment[];

  @OneToMany(() => File, (file) => file.task)
  files!: File[];

  @Column({ name: 'project_id', nullable: true })
  @Index()
  projectId!: string;

  @ManyToOne(() => Project, (project) => project.id, {
    onDelete: 'CASCADE',
    eager: true,
  })
  @JoinColumn({ name: 'project_id' })
  project?: Project;

  @Column({ name: 'user_id', nullable: true })
  @Index()
  userId!: string;

  @ManyToOne(() => UserEntity, (user) => user.tasks, {
    onDelete: 'CASCADE',
    eager: true,
  })
  @JoinColumn({ name: 'user_id' })
  user?: UserEntity;
}

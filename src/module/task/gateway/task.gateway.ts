import {
  WebSocketGateway,
  OnGatewayConnection,
  OnGatewayDisconnect,
  WebSocketServer,
} from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { TaskDto } from '../dto/task.dto';

@WebSocketGateway({ cors: true })
export class TaskGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  server: Server;

  handleConnection(client: Socket) {
    console.log(`Client connected: ${client.id}`);
  }

  handleDisconnect(client: Socket) {
    console.log(`Client disconnected: ${client.id}`);
  }

  async emitTaskCreated(task: TaskDto) {
    this.server.emit('taskCreated', task);
  }

  async emitTaskUpdated(task: TaskDto) {
    this.server.emit('taskUpdated', task);
  }

  async emitAssignedTask(task: TaskDto | null) {
    this.server.emit('assignedTask', task);
  }
}

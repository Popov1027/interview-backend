import { Test, TestingModule } from '@nestjs/testing';
import { TaskGateway } from './task.gateway';
import { Server, Socket } from 'socket.io';
import { TaskDto } from '../dto/task.dto';

const task = { id: '1', name: 'Task' } as TaskDto;

describe('TaskGateway', () => {
  let gateway: TaskGateway;
  let server: Server;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TaskGateway],
    }).compile();

    gateway = module.get<TaskGateway>(TaskGateway);
    server = {
      emit: jest.fn(),
    } as any as Server;
    gateway.server = server;
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });

  describe('handleConnection', () => {
    it('should log client connection', () => {
      const consoleSpy = jest.spyOn(console, 'log');
      const client = { id: 'client1' } as Socket;
      gateway.handleConnection(client);
      expect(consoleSpy).toHaveBeenCalledWith(`Client connected: ${client.id}`);
    });
  });

  describe('handleDisconnect', () => {
    it('should log client disconnection', () => {
      const consoleSpy = jest.spyOn(console, 'log');
      const client = { id: 'client1' } as Socket;
      gateway.handleDisconnect(client);
      expect(consoleSpy).toHaveBeenCalledWith(
        `Client disconnected: ${client.id}`,
      );
    });
  });

  describe('emitTaskCreated', () => {
    it('should emit taskCreated event with task', async () => {
      await gateway.emitTaskCreated(task);
      expect(server.emit).toHaveBeenCalledWith('taskCreated', task);
    });
  });

  describe('emitTaskUpdated', () => {
    it('should emit taskUpdated event with task', async () => {
      await gateway.emitTaskUpdated(task);
      expect(server.emit).toHaveBeenCalledWith('taskUpdated', task);
    });
  });

  describe('emitAssignedTask', () => {
    it('should emit assignedTask event with task', async () => {
      await gateway.emitAssignedTask(task);
      expect(server.emit).toHaveBeenCalledWith('assignedTask', task);
    });

    it('should emit assignedTask event with null', async () => {
      await gateway.emitAssignedTask(null);
      expect(server.emit).toHaveBeenCalledWith('assignedTask', null);
    });
  });
});

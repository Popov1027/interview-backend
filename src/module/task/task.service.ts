import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './entities/task.entity';
import { Like, Repository } from 'typeorm';
import {
  catchError,
  from,
  map,
  mergeMap,
  Observable,
  of,
  concatMap,
  take,
} from 'rxjs';
import { plainToInstance } from 'class-transformer';
import { TaskDto } from './dto/task.dto';
import { TaskGateway } from './gateway/task.gateway';
import { TwilioService } from 'nestjs-twilio';
import { TaskQueryDto } from './dto/task.query.dto';
import { RedisClient } from '../../core/redis/redis.client';
import { CacheService } from '../../core/redis/cache.service';
import { UserEntity } from '../user/entities/user.entity';
import { UserDecorator } from '../../core/auth/interface/token-payload.interface';

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(Task)
    private readonly taskRepository: Repository<Task>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @Inject('REDIS') private readonly redis: RedisClient,
    private readonly taskGateway: TaskGateway,
    private readonly twilioService: TwilioService,
    private readonly cacheManager: CacheService,
  ) {}

  create(createTaskDto: CreateTaskDto): Observable<TaskDto> {
    const task = this.taskRepository.create(createTaskDto);
    return from(this.taskRepository.save(task)).pipe(
      map((task) => {
        const taskDto = plainToInstance(TaskDto, task);
        from(this.taskGateway.emitTaskCreated(taskDto));
        from(this.cacheManager.invalidateCache('task_*'));
        return taskDto;
      }),
      catchError((error) => {
        throw new HttpException(
          `Failed to create task: ${error}`,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }),
    );
  }

  userAssignment(user: UserDecorator, taskId: string): Observable<TaskDto> {
    return from(this.userRepository.findOneBy({ id: user.userId })).pipe(
      take(1),
      mergeMap((user) => {
        if (!user) {
          throw new HttpException('User not found', HttpStatus.NOT_FOUND);
        }
        return from(this.taskRepository.findOneBy({ id: taskId })).pipe(
          take(1),
          mergeMap((task) => {
            if (!task) {
              throw new HttpException('Task not found', HttpStatus.NOT_FOUND);
            }

            if (task.userId === null) {
              task.userId = user.id;
              from(
                this.taskGateway.emitAssignedTask(
                  plainToInstance(TaskDto, task),
                ),
              );
              from(
                this.twilioService.client.messages.create({
                  body: `Task ${task.name} was assigned to ${user.firstName}`,
                  from: process.env.TWILIO_PHONE_NUMBER,
                  to: user.phoneNumber,
                }),
              );
            } else {
              from(this.taskGateway.emitAssignedTask(null));
              task.userId = null;
              task.user = null;
            }

            return from(this.taskRepository.save(task)).pipe(
              map((task) => {
                of(this.redis.del(`task_${taskId}`));
                return plainToInstance(TaskDto, task);
              }),
              catchError((error) => {
                throw new HttpException(
                  `Failed to assignment task: ${error}`,
                  HttpStatus.INTERNAL_SERVER_ERROR,
                );
              }),
            );
          }),
        );
      }),
    );
  }

  findAll({
    name,
    projectId,
    priority,
    status,
    sort,
  }: TaskQueryDto): Observable<TaskDto[]> {
    const cacheKey = `task_${name}_${projectId}_${priority}_${status}_${sort}`;
    return from(this.redis.get(cacheKey)).pipe(
      concatMap((cachedTasks) => {
        if (cachedTasks) {
          return of(cachedTasks);
        }
        return from(
          this.taskRepository.find({
            where: {
              ...(name && { name: Like(`%${name}%`) }),
              projectId,
              priority,
              status,
            },
            order: { priority: sort },
          }),
        ).pipe(
          map((tasks) => {
            const taskDto = tasks.map((task) => plainToInstance(TaskDto, task));
            from(this.redis.set(cacheKey, taskDto));
            return taskDto;
          }),
        );
      }),
    );
  }

  findOne(id: string): Observable<TaskDto> {
    const cacheKey = `task_${id}`;
    return from(this.redis.get(cacheKey)).pipe(
      concatMap((cachedTask) => {
        if (cachedTask) {
          return of(cachedTask);
        }
        return from(this.taskRepository.findOneBy({ id })).pipe(
          take(1),
          map((task) => {
            if (!task) {
              throw new HttpException(
                `Task with ID ${id} not found`,
                HttpStatus.NOT_FOUND,
              );
            }
            const taskDto = plainToInstance(TaskDto, task);
            of(this.redis.set(cacheKey, taskDto));
            return taskDto;
          }),
          catchError(() => {
            throw new HttpException(
              `Task with ID ${id} not found`,
              HttpStatus.NOT_FOUND,
            );
          }),
        );
      }),
    );
  }

  update(id: string, updateTaskDto: UpdateTaskDto): Observable<TaskDto> {
    return from(this.taskRepository.findOneBy({ id })).pipe(
      take(1),
      mergeMap((task) => {
        if (!task) {
          throw new HttpException('Task not found', HttpStatus.NOT_FOUND);
        }

        this.taskRepository.merge(task, updateTaskDto);
        return from(this.taskRepository.save(task)).pipe(
          map((task) => {
            const taskDto = plainToInstance(TaskDto, task);
            from(this.redis.set(`task_${id}`, taskDto));
            from(this.cacheManager.invalidateCache('task_*'));
            from(this.taskGateway.emitTaskUpdated(taskDto));
            return taskDto;
          }),
          catchError((error) => {
            throw new HttpException(
              `Failed to update task: ${error}`,
              HttpStatus.INTERNAL_SERVER_ERROR,
            );
          }),
        );
      }),
    );
  }

  remove(id: string): Observable<void> {
    return from(this.taskRepository.findOneBy({ id })).pipe(
      mergeMap((task) => {
        if (!task) {
          throw new HttpException(`Task not found`, HttpStatus.NOT_FOUND);
        }

        return from(this.taskRepository.remove(task)).pipe(
          map(() => {
            from(this.redis.del(`task_${id}`));
            from(this.cacheManager.invalidateCache('task_*'));
            return void 0;
          }),
          catchError((error) => {
            throw new HttpException(
              `Failed to remove task: ${error}`,
              HttpStatus.INTERNAL_SERVER_ERROR,
            );
          }),
        );
      }),
    );
  }
}

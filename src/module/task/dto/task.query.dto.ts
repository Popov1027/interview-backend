import { ApiProperty } from '@nestjs/swagger';
import {
  Sort,
  TaskPriority,
  TaskStatus,
} from '../../../common/enums/task.enum';

export class TaskQueryDto {
  @ApiProperty({
    type: String,
    required: false,
  })
  name: string;

  @ApiProperty({
    type: String,
    required: false,
  })
  projectId: string;

  @ApiProperty({
    type: 'enum',
    enum: TaskPriority,
    required: false,
  })
  priority: TaskPriority;

  @ApiProperty({
    type: 'enum',
    enum: TaskStatus,
    required: false,
  })
  status: TaskStatus;

  @ApiProperty({
    type: 'enum',
    enum: Sort,
    required: false,
  })
  sort: Sort;
}

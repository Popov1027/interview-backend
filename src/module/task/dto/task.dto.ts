import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import {
  IsArray,
  IsDate,
  IsInt,
  IsNotEmpty,
  IsObject,
  IsOptional,
  IsPositive,
  IsString,
} from 'class-validator';
import { TaskPriority, TaskStatus } from '../../../common/enums/task.enum';
import { Project } from '../../project/entities/project.entity';
import { Subactivity } from '../../subactivity/entities/subactivity.entity';
import { Comment } from '../../comment/entities/comment.entity';
import { File } from '../../file/entities/file.entity';
import { UserEntity } from '../../user/entities/user.entity';

@Exclude()
export class TaskDto {
  @ApiProperty({
    example: 'e13f257e-e37e-423b-a0a3-81cba53deec8',
    type: String,
  })
  @IsInt()
  @IsPositive()
  @Expose()
  id: string;

  @ApiProperty({
    example: '2022-01-22T13:55:37.839Z',
    type: Date,
  })
  @IsDate()
  @IsNotEmpty()
  @Expose()
  createdAt: Date;

  @ApiProperty({
    example: '2022-01-22T13:55:37.839Z',
    type: Date,
  })
  @IsDate()
  @IsNotEmpty()
  @Expose()
  updatedAt: Date;

  @ApiProperty({
    example: '2022-01-22T13:55:37.839Z',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  deadline: string;

  @ApiProperty({
    example: 'Atlanta',
    nullable: false,
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  name!: string;

  @ApiPropertyOptional({
    example:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    nullable: true,
  })
  @IsString()
  @IsOptional()
  @Expose()
  description: string;

  @ApiProperty({
    example: 'medium',
    type: 'enum',
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  priority: TaskPriority;

  @ApiProperty({
    example: 'todo',
    type: 'enum',
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  status: TaskStatus;

  @ApiPropertyOptional({
    example: 'e13f257e-e37e-423b-a0a3-81cba53deec8',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  projectId!: string;

  @ApiPropertyOptional({
    type: () => Project,
  })
  @Type(() => Project)
  @IsObject()
  @IsOptional()
  @Expose()
  project: Project;

  @ApiPropertyOptional({
    example: 'e13f257e-e37e-423b-a0a3-81cba53deec8',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  userId!: string;

  @ApiPropertyOptional({
    type: () => UserEntity,
  })
  @Type(() => UserEntity)
  @IsObject()
  @IsOptional()
  @Expose()
  user: UserEntity;

  @ApiPropertyOptional({
    type: () => Subactivity,
  })
  @Type(() => Subactivity)
  @IsArray()
  @IsOptional()
  @Expose()
  subactivities: Subactivity[];

  @ApiPropertyOptional({
    type: () => Comment,
  })
  @Type(() => Comment)
  @IsArray()
  @IsOptional()
  @Expose()
  comments: Comment[];

  @ApiPropertyOptional({
    type: () => File,
  })
  @Type(() => File)
  @IsArray()
  @IsOptional()
  @Expose()
  files: File[];
}

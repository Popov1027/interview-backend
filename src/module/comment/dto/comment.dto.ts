import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import {
  IsDate,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsObject,
  IsOptional,
  IsPositive,
  IsString,
} from 'class-validator';
import { Task } from '../../task/entities/task.entity';
import { Buffer } from '../../file/entities/buffer.entity';
import { UserEntity } from '../../user/entities/user.entity';

@Exclude()
export class CommentDto {
  @ApiProperty({
    example: 'e13f257e-e37e-423b-a0a3-81cba53deec8',
    type: String,
  })
  @IsInt()
  @IsPositive()
  @Expose()
  id: string;

  @ApiProperty({
    example: '2022-01-22T13:55:37.839Z',
    type: Date,
  })
  @IsDate()
  @IsNotEmpty()
  @Expose()
  createdAt: Date;

  @ApiProperty({
    example: '2022-01-22T13:55:37.839Z',
    type: Date,
  })
  @IsDate()
  @IsNotEmpty()
  @Expose()
  updatedAt: Date;

  @ApiProperty({
    example: 'Comment',
    nullable: false,
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  content: string;

  @ApiPropertyOptional({
    example: 'e13f257e-e37e-423b-a0a3-81cba53deec8',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  taskId!: string;

  @ApiPropertyOptional({
    type: () => Task,
  })
  @Type(() => Task)
  @IsObject()
  @IsOptional()
  @Expose()
  task: Task;

  @ApiPropertyOptional({
    example: 'e13f257e-e37e-423b-a0a3-81cba53deec8',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  userId!: string;

  @ApiPropertyOptional({
    type: () => UserEntity,
  })
  @Type(() => UserEntity)
  @IsObject()
  @IsOptional()
  @Expose()
  user: UserEntity;

  @ApiPropertyOptional({
    example: 'e13f257e-e37e-423b-a0a3-81cba53deec8',
    type: String,
  })
  @IsNumber()
  @IsNotEmpty()
  @Expose()
  bufferId!: number;

  @ApiPropertyOptional({
    type: () => Buffer,
  })
  @Type(() => Buffer)
  @IsObject()
  @IsOptional()
  @Expose()
  buffer: Buffer;
}

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateCommentDto {
  @ApiProperty({
    example: 'Comment',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  content: string;
}

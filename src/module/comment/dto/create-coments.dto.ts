import { IsArray, ValidateNested } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { CreateCommentDto } from './create-comment.dto';

export class CreateCommentsDto {
  @ApiProperty({
    type: [CreateCommentDto],
  })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateCommentDto)
  comments: CreateCommentDto[];
}

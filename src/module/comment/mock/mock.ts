import { of } from 'rxjs';
import { UpdateCommentDto } from '../dto/update-comment.dto';
import { CreateCommentDto } from '../dto/create-comment.dto';
import { CreateCommentsDto } from '../dto/create-coments.dto';
import { Task } from '../../task/entities/task.entity';
import { TaskPriority, TaskStatus } from '../../../common/enums/task.enum';
import { UserEntity } from '../../user/entities/user.entity';

const createCommentDto: CreateCommentDto = { content: 'Test comment' };
const createCommentsDto: CreateCommentsDto = { comments: [createCommentDto] };
const updateCommentDto: UpdateCommentDto = { content: 'Updated comment' };

const mockAuth: UserEntity = {
  id: '1',
  email: 'test@mail.com',
  firstName: 'First name',
  lastName: 'Last name',
  phoneNumber: '+37367528138',
  tasks: [],
  comments: [],
  password: 'Test123!',
};

const mockTask: Task = {
  id: '1',
  name: 'Test Task',
  description: 'Test Description',
  deadline: '11/11/2025',
  projectId: '1',
  priority: TaskPriority.MEDIUM,
  status: TaskStatus.IN_PROGRESS,
  createdAt: new Date(),
  updatedAt: new Date(),
  userId: '1',
  user: mockAuth,
  project: undefined,
  subactivities: [],
  comments: [],
  files: [],
};

const mockFile = {
  id: '1',
  createdAt: new Date(),
  filename: 'test.txt',
  path: 'https://example.com/test.txt',
  taskId: '1',
  buffers: [],
};

const mockComment = {
  id: '1',
  taskId: '1',
  userId: '1',
  content: 'Comment',
  createdAt: new Date(),
  updatedAt: new Date(),
};

const mockBuffer = {
  id: 1,
  content: 'Comment',
  comments: [mockComment],
  fileId: '1',
  file: { ...mockFile, task: mockTask },
};

const mockCommentRepository = {
  create: jest.fn().mockReturnValue(mockComment),
  save: jest.fn().mockResolvedValue(mockComment),
  findOneBy: jest.fn().mockResolvedValue(mockComment),
  find: jest.fn().mockResolvedValue([mockComment]),
  merge: jest.fn().mockResolvedValue(mockComment),
  remove: jest.fn().mockResolvedValue(undefined),
};

const mockAuthRepository = {
  findOneBy: jest.fn().mockResolvedValue(mockAuth),
};

const mockTaskRepository = {
  findOneBy: jest.fn().mockResolvedValue(mockTask),
};

const mockBufferRepository = {
  findOne: jest.fn().mockResolvedValue(mockBuffer),
};

const mockRedisClient = {
  get: jest.fn().mockResolvedValue(null),
  set: jest.fn().mockResolvedValue(null),
  del: jest.fn().mockResolvedValue(null),
};

const mockCommentService = {
  create: jest.fn().mockReturnValue(of([mockComment])),
  createByFileRow: jest.fn().mockReturnValue(of(mockComment)),
  findAll: jest.fn().mockReturnValue(of([mockComment])),
  findOne: jest.fn().mockReturnValue(of(mockComment)),
  update: jest.fn().mockReturnValue(of(mockComment)),
  remove: jest.fn().mockReturnValue(of(undefined)),
};

export {
  createCommentDto,
  createCommentsDto,
  updateCommentDto,
  mockComment,
  mockCommentService,
  mockCommentRepository,
  mockAuthRepository,
  mockTaskRepository,
  mockBufferRepository,
  mockRedisClient,
  mockTask,
  mockBuffer,
  mockAuth,
};

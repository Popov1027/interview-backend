import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import {
  catchError,
  concatMap,
  from,
  map,
  mergeMap,
  Observable,
  of,
  take,
} from 'rxjs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Task } from '../task/entities/task.entity';
import { Comment } from './entities/comment.entity';
import { plainToInstance } from 'class-transformer';
import { CommentDto } from './dto/comment.dto';
import { Buffer } from '../file/entities/buffer.entity';
import { CreateCommentsDto } from './dto/create-coments.dto';
import { RedisClient } from '../../core/redis/redis.client';
import { UserEntity } from '../user/entities/user.entity';
import { UserDecorator } from '../../core/auth/interface/token-payload.interface';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(Comment)
    private readonly commentRepository: Repository<Comment>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(Task)
    private readonly taskRepository: Repository<Task>,
    @InjectRepository(Buffer)
    private readonly bufferRepository: Repository<Buffer>,
    @Inject('REDIS') private readonly redis: RedisClient,
  ) {}

  create(
    taskId: string,
    user: UserDecorator,
    createCommentsDto: CreateCommentsDto,
  ): Observable<CommentDto[]> {
    return from(this.userRepository.findOneBy({ id: user.userId })).pipe(
      mergeMap((user) => {
        if (!user) {
          throw new HttpException('User not found', HttpStatus.NOT_FOUND);
        }
        return from(this.taskRepository.findOneBy({ id: taskId })).pipe(
          mergeMap((task) => {
            if (!task) {
              throw new HttpException('Task not found', HttpStatus.NOT_FOUND);
            }

            const newComments = createCommentsDto.comments.map((comment) =>
              this.commentRepository.create({
                ...comment,
                taskId: task.id,
                userId: user.id,
              }),
            );

            return from(this.commentRepository.save(newComments)).pipe(
              map((comments) => {
                const commentDto = plainToInstance(CommentDto, comments);
                from(this.redis.set('comment', commentDto));
                return commentDto;
              }),
              catchError((error) => {
                throw new HttpException(
                  `Failed to create comments: ${error}`,
                  HttpStatus.INTERNAL_SERVER_ERROR,
                );
              }),
            );
          }),
        );
      }),
    );
  }

  createByFileRow(
    bufferId: string,
    user: UserDecorator,
    createCommentDto: CreateCommentDto,
  ): Observable<CommentDto> {
    return from(this.userRepository.findOneBy({ id: user.userId })).pipe(
      mergeMap((user) => {
        if (!user) {
          throw new HttpException('User not found', HttpStatus.NOT_FOUND);
        }
        return from(
          this.bufferRepository.findOne({
            where: { id: bufferId },
            relations: ['file', 'file.task'],
          }),
        ).pipe(
          take(1),
          mergeMap((buffer) => {
            if (!buffer) {
              throw new HttpException('Buffer not found', HttpStatus.NOT_FOUND);
            }

            if (!buffer.file || !buffer.file.task) {
              throw new HttpException(
                'Associated task not found',
                HttpStatus.NOT_FOUND,
              );
            }

            const newComment = this.commentRepository.create({
              ...createCommentDto,
              bufferId: buffer.id,
              userId: user.id,
              taskId: buffer.file.task.id,
            });

            return from(this.commentRepository.save(newComment)).pipe(
              map((comment) => plainToInstance(CommentDto, comment)),
              catchError((error) => {
                throw new HttpException(
                  `Failed to create comment: ${error}`,
                  HttpStatus.INTERNAL_SERVER_ERROR,
                );
              }),
            );
          }),
        );
      }),
    );
  }

  findAll(user: UserDecorator): Observable<CommentDto[]> {
    const cacheKey = 'comment';
    return from(this.redis.get(cacheKey)).pipe(
      concatMap((cachedComment) => {
        if (cachedComment) {
          return of(cachedComment);
        }
        return from(
          this.commentRepository.find({ where: { userId: user.userId } }),
        ).pipe(
          map((comments) => {
            const commentDto = plainToInstance(CommentDto, comments);
            from(this.redis.set(cacheKey, commentDto));
            return commentDto;
          }),
        );
      }),
    );
  }

  findOne(id: string): Observable<CommentDto> {
    const cacheKey = `comment_${id}`;
    return from(this.redis.get(cacheKey)).pipe(
      concatMap((cachedComment) => {
        if (cachedComment) {
          return of(cachedComment);
        }
        return from(this.commentRepository.findOneBy({ id })).pipe(
          map((comment) => {
            if (!comment) {
              throw new HttpException(
                `Comment with ID ${id} not found`,
                HttpStatus.NOT_FOUND,
              );
            }
            const commentDto = plainToInstance(CommentDto, comment);
            of(this.redis.set(cacheKey, commentDto));
            return commentDto;
          }),
          catchError(() => {
            throw new HttpException(
              `Comment with ID ${id} not found`,
              HttpStatus.NOT_FOUND,
            );
          }),
        );
      }),
    );
  }

  update(
    id: string,
    updateCommentDto: UpdateCommentDto,
  ): Observable<CommentDto> {
    return from(this.commentRepository.findOneBy({ id })).pipe(
      mergeMap((comment) => {
        if (!comment) {
          throw new HttpException('Comment not found', HttpStatus.NOT_FOUND);
        }

        this.commentRepository.merge(comment, updateCommentDto);
        return from(this.commentRepository.save(comment)).pipe(
          map((comment) => {
            const commentDto = plainToInstance(CommentDto, comment);
            from(this.redis.set(`comment_${id}`, commentDto));
            return commentDto;
          }),
          catchError((error) => {
            throw new HttpException(
              `Failed to update comment: ${error}`,
              HttpStatus.INTERNAL_SERVER_ERROR,
            );
          }),
        );
      }),
    );
  }

  remove(id: string): Observable<void> {
    return from(this.commentRepository.findOneBy({ id })).pipe(
      mergeMap((comment) => {
        if (!comment) {
          throw new HttpException(`Comment not found`, HttpStatus.NOT_FOUND);
        }

        return from(this.commentRepository.remove(comment)).pipe(
          map(() => {
            from(this.redis.del(`comment_${id}`));
            return void 0;
          }),
          catchError((error) => {
            throw new HttpException(
              `Failed to remove comment: ${error}`,
              HttpStatus.INTERNAL_SERVER_ERROR,
            );
          }),
        );
      }),
      map(() => {
        return void 0;
      }),
    );
  }
}

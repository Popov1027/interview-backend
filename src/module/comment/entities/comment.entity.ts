import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Task } from '../../task/entities/task.entity';
import { Buffer } from '../../file/entities/buffer.entity';
import { UserEntity } from '../../user/entities/user.entity';

@Entity()
export class Comment {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
  })
  updatedAt: Date;

  @Column()
  content: string;

  @Column({ name: 'task_id' })
  @Index()
  taskId!: string;

  @ManyToOne(() => Task, (task) => task.comments, {
    onDelete: 'CASCADE',
    eager: true,
    nullable: false,
  })
  @JoinColumn({ name: 'task_id' })
  task?: Task;

  @Column({ name: 'user_id' })
  @Index()
  userId!: string;

  @ManyToOne(() => UserEntity, (user) => user.comments, {
    onDelete: 'CASCADE',
    eager: true,
  })
  @JoinColumn({ name: 'user_id' })
  user?: UserEntity;

  @Column({ name: 'buffer_id', nullable: true })
  @Index()
  bufferId!: string;

  @ManyToOne(() => Buffer, (buffer) => buffer.comments, {
    onDelete: 'CASCADE',
    eager: true,
  })
  @JoinColumn({ name: 'buffer_id' })
  buffer?: Buffer;
}

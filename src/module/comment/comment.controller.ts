import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  UseGuards,
  Query,
} from '@nestjs/common';
import { CommentService } from './comment.service';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { CommentDto } from './dto/comment.dto';
import { from, Observable } from 'rxjs';
import { CreateCommentsDto } from './dto/create-coments.dto';
import { JwtAuthGuard } from '../../core/auth/guards/auth.guard';
import { UserDecorator } from '../../core/auth/interface/token-payload.interface';
import { User } from '../user/dto/user.decorator';

@ApiTags('Comment')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('comment')
export class CommentController {
  constructor(private readonly commentService: CommentService) {}

  @ApiOperation({ summary: 'Endpoint to create comments' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Comments were created',
    type: [CommentDto],
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to create comments',
  })
  @ApiQuery({
    name: 'taskId',
    required: false,
    type: String,
  })
  @Post(':taskId')
  create(
    @Query('taskId') taskId: string,
    @User() user: UserDecorator,
    @Body() createCommentsDto: CreateCommentsDto,
  ): Observable<CommentDto[]> {
    return from(this.commentService.create(taskId, user, createCommentsDto));
  }

  @ApiOperation({ summary: 'Endpoint to create comment by file row' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Comment was created',
    type: CommentDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to create comment',
  })
  @ApiQuery({
    name: 'bufferId',
    required: true,
    type: String,
  })
  @Post('/row/:bufferId')
  createByFileRow(
    @Query('bufferId') bufferId: string,
    @User() user: UserDecorator,
    @Body() createCommentDto: CreateCommentDto,
  ): Observable<CommentDto> {
    return from(
      this.commentService.createByFileRow(bufferId, user, createCommentDto),
    );
  }

  @ApiOperation({ summary: 'Endpoint to get all comments by user ID' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Get comments',
    type: [CommentDto],
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to get comments',
  })
  @Get()
  findAll(@User() user: UserDecorator): Observable<CommentDto[]> {
    return from(this.commentService.findAll(user));
  }

  @ApiOperation({ summary: 'Endpoint to get comment by id' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Get comment by id',
    type: CommentDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to get comment by id',
  })
  @Get(':id')
  findOne(@Param('id') id: string): Observable<CommentDto> {
    return from(this.commentService.findOne(id));
  }

  @ApiOperation({ summary: 'Endpoint to update comment' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Update comment',
    type: CommentDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to update comment',
  })
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCommentDto: UpdateCommentDto,
  ): Observable<CommentDto> {
    return from(this.commentService.update(id, updateCommentDto));
  }

  @ApiOperation({ summary: 'Endpoint to delete comment' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Delete comment',
    type: CommentDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to delete comment',
  })
  @Delete(':id')
  remove(@Param('id') id: string): Observable<void> {
    return from(this.commentService.remove(id));
  }
}

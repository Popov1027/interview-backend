import { Test, TestingModule } from '@nestjs/testing';
import { CommentService } from './comment.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Comment } from './entities/comment.entity';
import { Auth } from '../auth/entities/user.entity';
import { Task } from '../task/entities/task.entity';
import { Buffer } from '../file/entities/buffer.entity';
import { HttpException, HttpStatus } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { CommentDto } from './dto/comment.dto';
import { user } from '../auth/mock/mock';
import {
  createCommentDto,
  createCommentsDto,
  mockAuth,
  mockAuthRepository,
  mockBuffer,
  mockBufferRepository,
  mockComment,
  mockCommentRepository,
  mockRedisClient,
  mockTask,
  mockTaskRepository,
  updateCommentDto,
} from './mock/mock';

describe('CommentService', () => {
  let service: CommentService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CommentService,
        {
          provide: getRepositoryToken(Comment),
          useValue: mockCommentRepository,
        },
        {
          provide: getRepositoryToken(Auth),
          useValue: mockAuthRepository,
        },
        {
          provide: getRepositoryToken(Task),
          useValue: mockTaskRepository,
        },
        {
          provide: getRepositoryToken(Buffer),
          useValue: mockBufferRepository,
        },
        { provide: 'REDIS', useValue: mockRedisClient },
      ],
    }).compile();

    service = module.get<CommentService>(CommentService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('should create comments', (done) => {
      jest
        .spyOn(mockAuthRepository, 'findOneBy')
        .mockResolvedValueOnce(mockAuth);
      jest
        .spyOn(mockTaskRepository, 'findOneBy')
        .mockResolvedValueOnce(mockTask);
      jest
        .spyOn(mockCommentRepository, 'save')
        .mockResolvedValueOnce([mockComment]);

      service.create('1', user, createCommentsDto).subscribe((result) => {
        expect(result).toEqual([plainToInstance(CommentDto, mockComment)]);
        expect(mockRedisClient.set).toHaveBeenCalled();
        done();
      });
    });

    it('should throw an error if user not found', (done) => {
      jest.spyOn(mockAuthRepository, 'findOneBy').mockResolvedValueOnce(null);

      service.create('1', user, createCommentsDto).subscribe({
        error: (error) => {
          expect(error).toBeInstanceOf(HttpException);
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if task not found', (done) => {
      jest
        .spyOn(mockAuthRepository, 'findOneBy')
        .mockResolvedValueOnce(mockAuth);
      jest.spyOn(mockTaskRepository, 'findOneBy').mockResolvedValueOnce(null);

      service.create('1', user, createCommentsDto).subscribe({
        error: (error) => {
          expect(error).toBeInstanceOf(HttpException);
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if comment creation fails', (done) => {
      jest
        .spyOn(mockAuthRepository, 'findOneBy')
        .mockResolvedValueOnce(mockAuth);
      jest
        .spyOn(mockTaskRepository, 'findOneBy')
        .mockResolvedValueOnce(mockTask);
      jest
        .spyOn(mockCommentRepository, 'save')
        .mockRejectedValueOnce(new Error('Create error'));

      service.create('1', user, createCommentsDto).subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to create comments: Error: Create error',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });

  describe('createByFileRow', () => {
    it('should create a comment', (done) => {
      jest
        .spyOn(mockAuthRepository, 'findOneBy')
        .mockResolvedValueOnce(mockAuth);
      jest
        .spyOn(mockBufferRepository, 'findOne')
        .mockResolvedValueOnce(mockBuffer);
      jest
        .spyOn(mockCommentRepository, 'save')
        .mockResolvedValueOnce(mockComment);

      service
        .createByFileRow('1', user, createCommentDto)
        .subscribe((result) => {
          expect(result).toEqual(plainToInstance(CommentDto, mockComment));
          done();
        });
    });

    it('should throw an error if user not found', (done) => {
      jest.spyOn(mockAuthRepository, 'findOneBy').mockResolvedValueOnce(null);

      service.createByFileRow('1', user, createCommentDto).subscribe({
        error: (error) => {
          expect(error).toBeInstanceOf(HttpException);
          expect(error.response).toBe('User not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if buffer not found', (done) => {
      jest
        .spyOn(mockAuthRepository, 'findOneBy')
        .mockResolvedValueOnce(mockAuth);
      jest.spyOn(mockBufferRepository, 'findOne').mockResolvedValueOnce(null);

      service.createByFileRow('1', user, createCommentDto).subscribe({
        error: (error) => {
          expect(error).toBeInstanceOf(HttpException);
          expect(error.response).toBe('Buffer not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if associated task not found', (done) => {
      jest
        .spyOn(mockAuthRepository, 'findOneBy')
        .mockResolvedValueOnce(mockAuth);
      jest.spyOn(mockBufferRepository, 'findOne').mockResolvedValueOnce({
        id: 1,
        file: null,
      });

      service.createByFileRow('1', user, createCommentDto).subscribe({
        error: (error) => {
          expect(error).toBeInstanceOf(HttpException);
          expect(error.response).toBe('Associated task not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if comment creation fails', (done) => {
      jest
        .spyOn(mockAuthRepository, 'findOneBy')
        .mockResolvedValueOnce(mockAuth);
      jest
        .spyOn(mockBufferRepository, 'findOne')
        .mockResolvedValueOnce(mockBuffer);
      jest
        .spyOn(mockCommentRepository, 'save')
        .mockRejectedValueOnce(new Error('Create error'));

      service.createByFileRow('1', user, createCommentDto).subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to create comment: Error: Create error',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });

  describe('findAll', () => {
    it('should return all comments from cache', (done) => {
      jest.spyOn(mockRedisClient, 'get').mockResolvedValueOnce([mockComment]);

      service.findAll(user).subscribe((result) => {
        expect(result).toEqual([mockComment]);
        done();
      });
    });

    it('should return all comments from database if not in cache', (done) => {
      jest.spyOn(mockRedisClient, 'get').mockResolvedValueOnce(null);
      jest
        .spyOn(mockCommentRepository, 'find')
        .mockResolvedValueOnce([mockComment]);

      service.findAll(user).subscribe((result) => {
        expect(result).toEqual([plainToInstance(CommentDto, mockComment)]);
        expect(mockRedisClient.set).toHaveBeenCalled();
        done();
      });
    });
  });

  describe('findOne', () => {
    it('should find one comment from cache', (done) => {
      jest.spyOn(mockRedisClient, 'get').mockResolvedValueOnce(mockComment);

      service.findOne('1').subscribe((result) => {
        expect(result).toEqual(mockComment);
        done();
      });
    });

    it('should find one comment from database if not in cache', (done) => {
      jest.spyOn(mockRedisClient, 'get').mockResolvedValueOnce(null);
      jest
        .spyOn(mockCommentRepository, 'findOneBy')
        .mockResolvedValueOnce(mockComment);

      service.findOne('1').subscribe((result) => {
        expect(result).toEqual(plainToInstance(CommentDto, mockComment));
        expect(mockRedisClient.set).toHaveBeenCalled();
        done();
      });
    });

    it('should throw an error if comment not found', (done) => {
      jest
        .spyOn(mockCommentRepository, 'findOneBy')
        .mockResolvedValueOnce(null);

      service.findOne('1').subscribe({
        error: (error) => {
          expect(error).toBeInstanceOf(HttpException);
          expect(error.response).toBe('Comment with ID 1 not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });
  });

  describe('update', () => {
    it('should update a comment', (done) => {
      jest
        .spyOn(mockCommentRepository, 'findOneBy')
        .mockResolvedValueOnce(mockComment);
      jest
        .spyOn(mockCommentRepository, 'save')
        .mockResolvedValueOnce(mockComment);

      service.update('1', updateCommentDto).subscribe((result) => {
        expect(result).toEqual(plainToInstance(CommentDto, mockComment));
        expect(mockRedisClient.set).toHaveBeenCalled();
        done();
      });
    });

    it('should throw an error if comment not found', (done) => {
      jest
        .spyOn(mockCommentRepository, 'findOneBy')
        .mockResolvedValueOnce(null);

      service.update('1', updateCommentDto).subscribe({
        error: (error) => {
          expect(error).toBeInstanceOf(HttpException);
          expect(error.response).toBe('Comment not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if update fails', (done) => {
      jest
        .spyOn(mockCommentRepository, 'findOneBy')
        .mockResolvedValueOnce(mockComment);
      jest
        .spyOn(mockCommentRepository, 'save')
        .mockRejectedValueOnce(new Error('Update error'));

      service.update('1', updateCommentDto).subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to update comment: Error: Update error',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });

  describe('remove', () => {
    it('should remove a comment', (done) => {
      jest
        .spyOn(mockCommentRepository, 'findOneBy')
        .mockResolvedValueOnce(mockComment);
      jest
        .spyOn(mockCommentRepository, 'remove')
        .mockResolvedValueOnce(undefined);

      service.remove('1').subscribe((result) => {
        expect(result).toBeUndefined();
        expect(mockRedisClient.del).toHaveBeenCalled();
        done();
      });
    });

    it('should throw an error if comment not found', (done) => {
      jest
        .spyOn(mockCommentRepository, 'findOneBy')
        .mockResolvedValueOnce(null);

      service.remove('1').subscribe({
        error: (error) => {
          expect(error).toBeInstanceOf(HttpException);
          expect(error.response).toBe('Comment not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if remove fails', (done) => {
      jest
        .spyOn(mockCommentRepository, 'findOneBy')
        .mockResolvedValueOnce(mockComment);
      jest
        .spyOn(mockCommentRepository, 'remove')
        .mockRejectedValueOnce(new Error('Remove error'));

      service.remove('1').subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to remove comment: Error: Remove error',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });
});

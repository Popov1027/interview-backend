import { Test, TestingModule } from '@nestjs/testing';
import { CommentController } from './comment.controller';
import { CommentService } from './comment.service';
import { CacheModule } from '@nestjs/cache-manager';
import { throwError } from 'rxjs';
import {
  createCommentDto,
  createCommentsDto,
  mockComment,
  mockCommentService,
  updateCommentDto,
} from './mock/mock';
import { user } from '../auth/mock/mock';

describe('CommentController', () => {
  let controller: CommentController;
  let service: CommentService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CacheModule.register()],
      controllers: [CommentController],
      providers: [
        {
          provide: CommentService,
          useValue: mockCommentService,
        },
      ],
    }).compile();

    controller = module.get<CommentController>(CommentController);
    service = module.get<CommentService>(CommentService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('create', () => {
    it('should create a comment', (done) => {
      controller.create('1', user, createCommentsDto).subscribe((result) => {
        expect(result).toEqual([mockComment]);
        expect(service.create).toHaveBeenCalledWith(
          '1',
          user,
          createCommentsDto,
        );
        done();
      });
    });

    it('should handle error when creating comment', (done) => {
      jest
        .spyOn(service, 'create')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to create comment')),
        );
      controller.create('1', user, createCommentsDto).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to create comment');
          done();
        },
      });
    });
  });

  describe('createByFileRow', () => {
    it('should create a comment by file row', (done) => {
      controller
        .createByFileRow('1', user, createCommentDto)
        .subscribe((result) => {
          expect(result).toEqual(mockComment);
          expect(service.createByFileRow).toHaveBeenCalledWith(
            1,
            user,
            createCommentDto,
          );
          done();
        });
    });

    it('should handle error when creating comment by file row', (done) => {
      jest
        .spyOn(service, 'createByFileRow')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to create comment by file row')),
        );
      controller.createByFileRow('1', user, createCommentDto).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to create comment by file row');
          done();
        },
      });
    });
  });

  describe('findAll', () => {
    it('should find all comments', (done) => {
      controller.findAll(user).subscribe((result) => {
        expect(result).toEqual([mockComment]);
        expect(service.findAll).toHaveBeenCalledWith(user);
        done();
      });
    });

    it('should handle error when finding all comments', (done) => {
      jest
        .spyOn(service, 'findAll')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to get all comments')),
        );

      controller.findAll(user).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to get all comments');
          done();
        },
      });
    });
  });

  describe('findOne', () => {
    it('should find one comment by id', (done) => {
      controller.findOne('1').subscribe((result) => {
        expect(result).toEqual(mockComment);
        expect(service.findOne).toHaveBeenCalledWith('1');
        done();
      });
    });

    it('should handle error when finding comment by id', (done) => {
      jest
        .spyOn(service, 'findOne')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to get comment by id')),
        );
      controller.findOne('1').subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to get comment by id');
          done();
        },
      });
    });
  });

  describe('update', () => {
    it('should update a comment', (done) => {
      controller.update('1', updateCommentDto).subscribe((result) => {
        expect(result).toEqual(mockComment);
        expect(service.update).toHaveBeenCalledWith('1', updateCommentDto);
        done();
      });
    });

    it('should handle error when updating comment', (done) => {
      jest
        .spyOn(service, 'update')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to update comment')),
        );
      controller.update('1', updateCommentDto).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to update comment');
          done();
        },
      });
    });
  });

  describe('remove', () => {
    it('should remove a comment', (done) => {
      controller.remove('1').subscribe((result) => {
        expect(result).toBeUndefined();
        expect(service.remove).toHaveBeenCalledWith('1');
        done();
      });
    });

    it('should handle error when removing comment', (done) => {
      jest
        .spyOn(service, 'remove')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to delete comment')),
        );
      controller.remove('1').subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to delete comment');
          done();
        },
      });
    });
  });
});

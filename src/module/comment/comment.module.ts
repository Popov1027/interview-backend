import { Module } from '@nestjs/common';
import { CommentService } from './comment.service';
import { CommentController } from './comment.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Comment } from './entities/comment.entity';
import { Task } from '../task/entities/task.entity';
import { Buffer } from '../file/entities/buffer.entity';
import { UserEntity } from '../user/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Comment, UserEntity, Task, Buffer])],
  controllers: [CommentController],
  providers: [CommentService],
})
export class CommentModule {}

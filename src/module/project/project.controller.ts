import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  UseGuards,
  UseInterceptors,
  UploadedFile,
  ParseFilePipeBuilder,
  Query,
} from '@nestjs/common';
import { ProjectService } from './project.service';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ProjectDto } from './dto/project.dto';
import { from, Observable } from 'rxjs';
import { FileInterceptor } from '@nestjs/platform-express';
import { ProjectsDto } from './dto/projects.dto';
import { ProjectQueryDto } from './dto/project.query.dto';
import { JwtAuthGuard } from '../../core/auth/guards/auth.guard';

@ApiTags('Project')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('project')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}

  @ApiOperation({ summary: 'Endpoint to create project' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Project was created',
    type: ProjectDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to create project',
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'Project data',
    required: true,
    schema: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
          description: 'Name of the project',
        },
        description: {
          type: 'string',
          description: 'Description of the project',
        },
        file: {
          type: 'string',
          format: 'binary',
          description: 'File to be uploaded',
        },
      },
      required: ['name', 'description'],
    },
  })
  @UseInterceptors(FileInterceptor('file'))
  @Post()
  create(
    @Body() createProjectDto: CreateProjectDto,
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addMaxSizeValidator({ maxSize: 1024 * 1024 })
        .addFileTypeValidator({ fileType: /.(jpg|jpeg|png|jfif)$/ })
        .build({
          errorHttpStatusCode: HttpStatus.BAD_REQUEST,
          fileIsRequired: false,
        }),
    )
    file?: Express.Multer.File,
  ): Observable<ProjectDto> {
    return from(this.projectService.create(createProjectDto, file));
  }

  @ApiOperation({ summary: 'Endpoint to get all projects' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get all projects',
    type: ProjectsDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to get all projects',
  })
  @Get()
  findAll(@Query() projectQueryDto: ProjectQueryDto): Observable<ProjectsDto> {
    return from(this.projectService.findAll(projectQueryDto));
  }

  @ApiOperation({ summary: 'Endpoint to get project by id' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Get project by id',
    type: ProjectDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to get project by id',
  })
  @Get(':id')
  findOne(@Param('id') id: string): Observable<ProjectDto> {
    return from(this.projectService.findOne(id));
  }

  @ApiOperation({ summary: 'Endpoint to update project' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Project was updated',
    type: ProjectDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to update project',
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'Project data',
    schema: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
          description: 'Name of the project',
        },
        description: {
          type: 'string',
          description: 'Description of the project',
        },
        file: {
          type: 'string',
          format: 'binary',
          description: 'File to be uploaded',
        },
      },
    },
  })
  @UseInterceptors(FileInterceptor('file'))
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateProjectDto: UpdateProjectDto,
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addMaxSizeValidator({ maxSize: 1024 * 1024 })
        .addFileTypeValidator({ fileType: /.(jpg|jpeg|png|jfif)$/ })
        .build({
          errorHttpStatusCode: HttpStatus.BAD_REQUEST,
          fileIsRequired: false,
        }),
    )
    file?: Express.Multer.File,
  ): Observable<ProjectDto> {
    return from(this.projectService.update(id, updateProjectDto, file));
  }

  @ApiOperation({ summary: 'Endpoint to delete project' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Delete project',
    type: ProjectDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to delete project',
  })
  @Delete(':id')
  remove(@Param('id') id: string): Observable<void> {
    return from(this.projectService.remove(id));
  }
}

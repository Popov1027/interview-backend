import { ProjectQueryDto } from '../dto/project.query.dto';
import { CreateProjectDto } from '../dto/create-project.dto';
import { of } from 'rxjs';

const queryDto: ProjectQueryDto = {
  name: 'Project',
  page: 1,
  limit: 10,
};

const createProjectDto: CreateProjectDto = {
  name: 'Test Project',
  description: 'Test Description',
};

const updateProjectDto = { ...createProjectDto };

const mockProject = {
  id: '1',
  name: 'Test Project',
  description: 'Test Description',
  image: '/image/path',
  createdAt: new Date(),
  updatedAt: new Date(),
  tasks: [],
};

const mockProjectService = {
  create: jest.fn().mockReturnValue(of(mockProject)),
  findAll: jest.fn().mockReturnValue(of([mockProject])),
  findOne: jest.fn().mockReturnValue(of(mockProject)),
  update: jest.fn().mockReturnValue(of(mockProject)),
  remove: jest.fn().mockReturnValue(of(undefined)),
};

const mockProjectRepository = {
  create: jest.fn().mockReturnValue(mockProject),
  save: jest.fn().mockResolvedValue(mockProject),
  findOneBy: jest.fn().mockResolvedValue(mockProject),
  findAndCount: jest.fn().mockResolvedValue([[mockProject], 1]),
  remove: jest.fn().mockResolvedValue(undefined),
  merge: jest.fn(),
};

const mockStorageService = {
  upload: jest.fn().mockResolvedValue({ url: 'https://example.com/image.jpg' }),
};

const mockRedisClient = {
  get: jest.fn().mockResolvedValue(null),
  set: jest.fn().mockResolvedValue(null),
  del: jest.fn().mockResolvedValue(null),
};

const mockCacheService = {
  invalidateCache: jest.fn().mockResolvedValue(null),
};

export {
  queryDto,
  createProjectDto,
  updateProjectDto,
  mockProject,
  mockProjectService,
  mockProjectRepository,
  mockRedisClient,
  mockCacheService,
  mockStorageService,
};

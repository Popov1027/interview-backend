import { Test, TestingModule } from '@nestjs/testing';
import { ProjectController } from './project.controller';
import { ProjectService } from './project.service';
import { CacheModule } from '@nestjs/cache-manager';
import { throwError } from 'rxjs';
import {
  createProjectDto,
  mockProject,
  mockProjectService,
  queryDto,
  updateProjectDto,
} from './mock/mock';
import { file } from '../file/mock/mock';

describe('ProjectController', () => {
  let controller: ProjectController;
  let service: ProjectService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CacheModule.register()],
      controllers: [ProjectController],
      providers: [
        {
          provide: ProjectService,
          useValue: mockProjectService,
        },
      ],
    }).compile();

    controller = module.get<ProjectController>(ProjectController);
    service = module.get<ProjectService>(ProjectService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('create', () => {
    it('should create a project', (done) => {
      controller.create(createProjectDto, file).subscribe((result) => {
        expect(result).toEqual(mockProject);
        expect(service.create).toHaveBeenCalledWith(createProjectDto, file);
        done();
      });
    });

    it('should handle error when creating project', (done) => {
      jest
        .spyOn(service, 'create')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to create project')),
        );
      controller.create(createProjectDto, file).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to create project');
          done();
        },
      });
    });
  });

  describe('findAll', () => {
    it('should find all projects', (done) => {
      controller.findAll(queryDto).subscribe((result) => {
        expect(result).toEqual([mockProject]);
        expect(service.findAll).toHaveBeenCalledWith(queryDto);
        done();
      });
    });

    it('should handle error when finding all projects', (done) => {
      jest
        .spyOn(service, 'findAll')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to get all projects')),
        );

      controller.findAll(queryDto).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to get all projects');
          done();
        },
      });
    });
  });

  describe('findOne', () => {
    it('should find one project by id', (done) => {
      controller.findOne('1').subscribe((result) => {
        expect(result).toEqual(mockProject);
        expect(service.findOne).toHaveBeenCalledWith('1');
        done();
      });
    });

    it('should handle error when finding project by id', (done) => {
      jest
        .spyOn(service, 'findOne')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to get project by id')),
        );
      controller.findOne('1').subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to get project by id');
          done();
        },
      });
    });
  });

  describe('update', () => {
    it('should update a project', (done) => {
      controller.update('1', updateProjectDto, file).subscribe((result) => {
        expect(result).toEqual(mockProject);
        expect(service.update).toHaveBeenCalledWith(
          '1',
          updateProjectDto,
          file,
        );
        done();
      });
    });

    it('should handle error when updating project', (done) => {
      jest
        .spyOn(service, 'update')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to update project')),
        );
      controller.update('1', updateProjectDto, file).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to update project');
          done();
        },
      });
    });
  });

  describe('remove', () => {
    it('should remove a project', (done) => {
      controller.remove('1').subscribe((result) => {
        expect(result).toBeUndefined();
        expect(service.remove).toHaveBeenCalledWith('1');
        done();
      });
    });

    it('should handle error when removing project', (done) => {
      jest
        .spyOn(service, 'remove')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to delete project')),
        );
      controller.remove('1').subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to delete project');
          done();
        },
      });
    });
  });
});

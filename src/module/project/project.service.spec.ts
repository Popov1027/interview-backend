import { Test, TestingModule } from '@nestjs/testing';
import { ProjectService } from './project.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Project } from './entities/project.entity';
import { CacheService } from '../../core/redis/cache.service';
import { Repository } from 'typeorm';
import { HttpStatus } from '@nestjs/common';
import {
  createProjectDto,
  mockCacheService,
  mockProject,
  mockProjectRepository,
  mockRedisClient,
  mockStorageService,
  queryDto,
  updateProjectDto,
} from './mock/mock';
import { file } from '../file/mock/mock';

describe('ProjectService', () => {
  let service: ProjectService;
  let repository: Repository<Project>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProjectService,
        {
          provide: getRepositoryToken(Project),
          useValue: mockProjectRepository,
        },
        { provide: 'S3', useValue: mockStorageService },
        { provide: 'REDIS', useValue: mockRedisClient },
        { provide: CacheService, useValue: mockCacheService },
      ],
    }).compile();

    service = module.get<ProjectService>(ProjectService);
    repository = module.get<Repository<Project>>(getRepositoryToken(Project));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('should create a project with an image', (done) => {
      service.create(createProjectDto, file).subscribe((result) => {
        expect(result).toEqual(mockProject);
        expect(mockCacheService.invalidateCache).toHaveBeenCalledWith(
          'project_*',
        );
        done();
      });
    });

    it('should create a project without an image', (done) => {
      service.create(createProjectDto, null).subscribe((result) => {
        expect(result).toEqual(mockProject);
        expect(mockCacheService.invalidateCache).toHaveBeenCalledWith(
          'project_*',
        );
        done();
      });
    });

    it('should throw an error if creation fails', (done) => {
      jest
        .spyOn(repository, 'save')
        .mockRejectedValueOnce(new Error('Error creating project'));
      service.create(createProjectDto, null).subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to create project: Error: Error creating project',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });

  describe('findAll', () => {
    it('should find all projects', (done) => {
      service.findAll(queryDto).subscribe((result) => {
        expect(result.projects).toEqual([mockProject]);
        done();
      });
    });

    it('should return cached projects', (done) => {
      jest.spyOn(mockRedisClient, 'get').mockResolvedValueOnce([mockProject]);
      service.findAll(queryDto).subscribe((result) => {
        expect(result.projects).toEqual([mockProject]);
        done();
      });
    });
  });

  describe('findOne', () => {
    it('should find one project', (done) => {
      service.findOne('1').subscribe((result) => {
        expect(result).toEqual(mockProject);
        done();
      });
    });

    it('should return cached project', (done) => {
      jest.spyOn(mockRedisClient, 'get').mockResolvedValueOnce(mockProject);
      service.findOne('1').subscribe((result) => {
        expect(result).toEqual(mockProject);
        done();
      });
    });

    it('should throw an error if project not found', (done) => {
      jest.spyOn(repository, 'findOneBy').mockResolvedValueOnce(null);
      service.findOne('1').subscribe({
        error: (error) => {
          expect(error.response).toBe('Project with ID 1 not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });
  });

  describe('update', () => {
    it('should update a project with an image', (done) => {
      service.update('1', updateProjectDto, file).subscribe((result) => {
        expect(result).toEqual(mockProject);
        expect(mockCacheService.invalidateCache).toHaveBeenCalledWith(
          'project_*',
        );
        done();
      });
    });

    it('should update a project without an image', (done) => {
      service.update('1', updateProjectDto, null).subscribe((result) => {
        expect(result).toEqual(mockProject);
        expect(mockCacheService.invalidateCache).toHaveBeenCalledWith(
          'project_*',
        );
        done();
      });
    });

    it('should throw an error if project not found', (done) => {
      jest.spyOn(repository, 'findOneBy').mockResolvedValueOnce(null);
      service.update('1', updateProjectDto, null).subscribe({
        error: (error) => {
          expect(error.response).toBe('Project not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if failed to update project', (done) => {
      jest.spyOn(repository, 'findOneBy').mockResolvedValueOnce(mockProject);
      jest
        .spyOn(repository, 'save')
        .mockRejectedValueOnce(new Error('Error updating project'));
      service.update('1', updateProjectDto, null).subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to update project: Error: Error updating project',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });

  describe('remove', () => {
    it('should remove a project', (done) => {
      service.remove('1').subscribe((result) => {
        expect(result).toBeUndefined();
        expect(mockRedisClient.del).toHaveBeenCalledWith('project_1');
        expect(mockCacheService.invalidateCache).toHaveBeenCalledWith(
          'project_*',
        );
        done();
      });
    });

    it('should throw an error if project not found', (done) => {
      jest.spyOn(repository, 'findOneBy').mockResolvedValueOnce(null);
      service.remove('1').subscribe({
        error: (error) => {
          expect(error.response).toBe('Project not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if failed to remove project', (done) => {
      jest.spyOn(repository, 'findOneBy').mockResolvedValueOnce(mockProject);
      jest
        .spyOn(repository, 'remove')
        .mockRejectedValueOnce(new Error('Error removing project'));
      service.remove('1').subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to remove project: Error: Error removing project',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });
});

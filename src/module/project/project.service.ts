import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { Project } from './entities/project.entity';
import { Like, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import {
  catchError,
  concatMap,
  from,
  map,
  mergeMap,
  Observable,
  of,
  take,
} from 'rxjs';
import { plainToInstance } from 'class-transformer';
import { ProjectDto } from './dto/project.dto';
import { ProjectsDto } from './dto/projects.dto';
import { IStorageService } from '../../common/interfaces/is-storage-service';
import { ProjectQueryDto } from './dto/project.query.dto';
import { RedisClient } from '../../core/redis/redis.client';
import { CacheService } from '../../core/redis/cache.service';

@Injectable()
export class ProjectService {
  constructor(
    @InjectRepository(Project)
    private readonly projectRepository: Repository<Project>,
    @Inject('S3') private readonly storageService: IStorageService,
    @Inject('REDIS') private readonly redis: RedisClient,
    private readonly cacheManager: CacheService,
  ) {}

  create(
    createProjectDto: CreateProjectDto,
    file?: Express.Multer.File,
  ): Observable<ProjectDto> {
    const project = this.projectRepository.create(createProjectDto);

    const saveProject$ = (imagePath?: {
      url: string;
    }): Observable<ProjectDto> => {
      if (imagePath) {
        project.image = imagePath.url;
      }
      return from(this.projectRepository.save(project)).pipe(
        map((result) => {
          const projectDto = plainToInstance(ProjectDto, result);
          from(this.cacheManager.invalidateCache('project_*'));
          return projectDto;
        }),
        catchError((error) => {
          throw new HttpException(
            `Failed to create project: ${error}`,
            HttpStatus.INTERNAL_SERVER_ERROR,
          );
        }),
      );
    };

    if (file) {
      return from(this.storageService.upload(file)).pipe(
        concatMap((uploadedFilePath) => saveProject$(uploadedFilePath)),
      );
    }
    return saveProject$();
  }

  findAll({ name, page, limit }: ProjectQueryDto): Observable<ProjectsDto> {
    const cacheKey = `project_${name}_${page}_${limit}`;
    return from(this.redis.get(cacheKey)).pipe(
      concatMap((cachedProjects) => {
        if (cachedProjects) {
          return of({
            page,
            limit,
            total: cachedProjects.length,
            projects: cachedProjects,
          });
        }
        return from(
          this.projectRepository.findAndCount({
            where: { ...(name && { name: Like(`%${name}%`) }) },
            skip: (page - 1) * limit,
            take: limit,
          }),
        ).pipe(
          map(([projects, total]) => {
            const projectDto = plainToInstance(ProjectDto, projects);
            from(this.redis.set(cacheKey, projectDto));
            return {
              page,
              limit,
              total,
              projects: projectDto,
            };
          }),
        );
      }),
    );
  }

  findOne(id: string): Observable<ProjectDto> {
    const cacheKey = `project_${id}`;
    return from(this.redis.get(cacheKey)).pipe(
      concatMap((cachedProject) => {
        if (cachedProject) {
          return of(cachedProject);
        }
        return from(this.projectRepository.findOneBy({ id })).pipe(
          take(1),
          map((project) => {
            if (!project) {
              throw new HttpException(
                `Project with ID ${id} not found`,
                HttpStatus.NOT_FOUND,
              );
            }
            const projectDto = plainToInstance(ProjectDto, project);
            from(this.redis.set(cacheKey, projectDto));
            return projectDto;
          }),
          catchError(() => {
            throw new HttpException(
              `Project with ID ${id} not found`,
              HttpStatus.NOT_FOUND,
            );
          }),
        );
      }),
    );
  }

  update(
    id: string,
    updateProjectDto: UpdateProjectDto,
    file?: Express.Multer.File,
  ): Observable<ProjectDto> {
    return from(this.projectRepository.findOneBy({ id })).pipe(
      take(1),
      mergeMap((project) => {
        if (!project) {
          throw new HttpException('Project not found', HttpStatus.NOT_FOUND);
        }

        const updateProject$ = (imagePath?: {
          url: string;
        }): Observable<ProjectDto> => {
          if (imagePath) {
            project.image = imagePath.url;
          }
          this.projectRepository.merge(project, updateProjectDto);
          return from(this.projectRepository.save(project)).pipe(
            map((result) => {
              const projectDto = plainToInstance(ProjectDto, result);
              from(this.cacheManager.invalidateCache('project_*'));
              return projectDto;
            }),
            catchError((error) => {
              throw new HttpException(
                `Failed to update project: ${error}`,
                HttpStatus.INTERNAL_SERVER_ERROR,
              );
            }),
          );
        };

        if (file) {
          return from(this.storageService.upload(file)).pipe(
            mergeMap((uploadedFilePath) => updateProject$(uploadedFilePath)),
          );
        }
        return updateProject$();
      }),
    );
  }

  remove(id: string): Observable<void> {
    return from(this.projectRepository.findOneBy({ id })).pipe(
      mergeMap((project) => {
        if (!project) {
          throw new HttpException(`Project not found`, HttpStatus.NOT_FOUND);
        }

        return from(this.projectRepository.remove(project)).pipe(
          map(() => {
            from(this.redis.del(`project_${id}`));
            from(this.cacheManager.invalidateCache('project_*'));
            return void 0;
          }),
          catchError((error) => {
            throw new HttpException(
              `Failed to remove project: ${error}`,
              HttpStatus.INTERNAL_SERVER_ERROR,
            );
          }),
        );
      }),
    );
  }
}

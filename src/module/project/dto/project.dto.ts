import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import {
  IsArray,
  IsDate,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsPositive,
  IsString,
} from 'class-validator';
import { TaskDto } from '../../task/dto/task.dto';

@Exclude()
export class ProjectDto {
  @ApiProperty({
    example: 'e13f257e-e37e-423b-a0a3-81cba53deec8',
    type: String,
  })
  @IsInt()
  @IsPositive()
  @Expose()
  id: string;

  @ApiProperty({
    example: '2022-01-22T13:55:37.839Z',
    type: Date,
  })
  @IsDate()
  @IsNotEmpty()
  @Expose()
  createdAt: Date;

  @ApiProperty({
    example: '2022-01-22T13:55:37.839Z',
    type: Date,
  })
  @IsDate()
  @IsNotEmpty()
  @Expose()
  updatedAt: Date;

  @ApiProperty({
    example: 'Atlanta',
    nullable: false,
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  name: string;

  @ApiPropertyOptional({
    example:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    nullable: true,
  })
  @IsString()
  @IsOptional()
  @Expose()
  description: string;

  @ApiPropertyOptional({
    example:
      'https://interview-issue-tracker.s3.eu-central-1.amazonaws.com/5956592.png',
    nullable: true,
  })
  @IsString()
  @Expose()
  image?: string;

  @ApiPropertyOptional({
    type: () => TaskDto,
    isArray: true,
  })
  @Type(() => TaskDto)
  @IsArray()
  @IsOptional()
  @Expose()
  tasks: TaskDto[];
}

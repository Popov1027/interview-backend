import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, IsInt, IsNotEmpty, IsPositive } from 'class-validator';
import { ProjectDto } from './project.dto';

export class ProjectsDto {
  @ApiProperty({
    example: 1,
    description: 'Current page number',
  })
  @IsInt()
  @IsPositive()
  @IsNotEmpty()
  page: number;

  @ApiProperty({
    example: 10,
    description: 'Number of items per page',
  })
  @IsInt()
  @IsPositive()
  @IsNotEmpty()
  limit: number;

  @ApiProperty({
    example: 100,
    description: 'Total number of items',
  })
  @IsInt()
  @IsNotEmpty()
  total: number;

  @ApiProperty({
    type: [ProjectDto],
    description: 'Array of project items',
  })
  @IsArray()
  @Type(() => ProjectDto)
  @IsNotEmpty()
  projects: ProjectDto[];
}

import { ApiProperty } from '@nestjs/swagger';

export class ProjectQueryDto {
  @ApiProperty({
    type: String,
    required: false,
  })
  name: string;

  @ApiProperty({
    type: Number,
    required: true,
  })
  page: number;

  @ApiProperty({
    type: Number,
    required: true,
  })
  limit: number;
}

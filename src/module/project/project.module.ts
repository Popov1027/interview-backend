import { Module } from '@nestjs/common';
import { ProjectService } from './project.service';
import { ProjectController } from './project.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Project } from './entities/project.entity';
import { S3Service } from '../file/storage/s3.service';
import { CacheService } from '../../core/redis/cache.service';

@Module({
  imports: [TypeOrmModule.forFeature([Project])],
  controllers: [ProjectController],
  providers: [
    ProjectService,
    CacheService,
    {
      provide: 'S3',
      useClass: S3Service,
    },
  ],
})
export class ProjectModule {}

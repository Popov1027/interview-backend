import { ProjectModule } from './project/project.module';
import { TaskModule } from './task/task.module';
import { SubactivityModule } from './subactivity/subactivity.module';
import { CommentModule } from './comment/comment.module';
import { FileModule } from './file/file.module';
import { AuthModule } from '../core/auth/auth.module';
import { UserModule } from './user/user.module';
import { GoogleModule } from '../core/google/google.module';

export const Modules = [
  AuthModule,
  UserModule,
  ProjectModule,
  TaskModule,
  SubactivityModule,
  CommentModule,
  FileModule,
  GoogleModule,
];

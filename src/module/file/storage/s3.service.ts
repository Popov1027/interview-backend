import { Injectable } from '@nestjs/common';
import {
  PutObjectCommand,
  DeleteObjectCommand,
  S3Client,
} from '@aws-sdk/client-s3';
import { catchError, from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IStorageService } from '../../../common/interfaces/is-storage-service';

@Injectable()
export class S3Service implements IStorageService {
  private readonly s3Client: S3Client;

  constructor() {
    this.s3Client = new S3Client({
      region: process.env.AWS_S3_REGION,
      credentials: {
        accessKeyId: process.env.AWS_ACCESS_KEY,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
      },
    });
  }

  upload(file: Express.Multer.File): Observable<{ url: string }> {
    return from(
      this.s3Client.send(
        new PutObjectCommand({
          Bucket: process.env.AWS_S3_BUCKET_NAME,
          Key: file.originalname,
          Body: file.buffer,
        }),
      ),
    ).pipe(
      catchError((error) => {
        throw error;
      }),
      map(() => ({
        url: `https://${process.env.AWS_S3_BUCKET_NAME}.s3.${process.env.AWS_S3_REGION}.amazonaws.com/${file.originalname}`,
      })),
    );
  }

  deleteFile(filename: string): Observable<any> {
    return from(
      this.s3Client.send(
        new DeleteObjectCommand({
          Bucket: process.env.AWS_S3_BUCKET_NAME,
          Key: filename,
        }),
      ),
    ).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }
}

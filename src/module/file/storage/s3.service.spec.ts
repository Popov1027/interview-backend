import { Test, TestingModule } from '@nestjs/testing';
import { MinioService } from './minio.service';
import * as Minio from 'minio';

jest.mock('minio');

describe('MinioService', () => {
  let service: MinioService;
  let minioClient: jest.Mocked<Minio.Client>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MinioService],
    }).compile();

    service = module.get<MinioService>(MinioService);
    minioClient = Minio.Client
      .prototype as unknown as jest.Mocked<Minio.Client>;
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('ensureBucket', () => {
    it('should return true if bucket exists', (done) => {
      minioClient.bucketExists = jest.fn().mockResolvedValue(true);

      service.ensureBucket().subscribe((result) => {
        expect(result).toBe(true);
        done();
      });
    });

    it('should create a bucket if it does not exist', (done) => {
      minioClient.bucketExists = jest
        .fn()
        .mockRejectedValue({ code: 'NoSuchBucket' });
      minioClient.makeBucket = jest.fn().mockResolvedValue(undefined);

      service.ensureBucket().subscribe(() => {
        expect(minioClient.makeBucket).toHaveBeenCalled();
        done();
      });
    });

    it('should throw an error if bucketExists fails with an unknown error', (done) => {
      minioClient.bucketExists = jest
        .fn()
        .mockRejectedValue({ code: 'UnknownError' });

      service.ensureBucket().subscribe({
        error: (error) => {
          expect(error.code).toBe('UnknownError');
          done();
        },
      });
    });
  });

  describe('upload', () => {
    it('should upload a file and return its URL', (done) => {
      const file = {
        originalname: 'test.pdf',
        buffer: Buffer.from('file content'),
        size: 1234,
      } as Express.Multer.File;

      minioClient.putObject = jest.fn().mockImplementation(() => {
        return Promise.resolve('etag');
      });

      service.upload(file).subscribe((result) => {
        expect(result.url).toBe(
          `http://localhost:9000/${process.env.MINIO_BUCKET}/test.pdf`,
        );
        done();
      });
    });

    it('should throw an error if upload fails', (done) => {
      const file = {
        originalname: 'test.pdf',
        buffer: Buffer.from('file content'),
        size: 1234,
      } as Express.Multer.File;

      minioClient.putObject = jest.fn().mockImplementation(() => {
        return Promise.reject(new Error('Upload failed'));
      });

      service.upload(file).subscribe({
        error: (error) => {
          expect(error.message).toBe('Upload failed');
          done();
        },
      });
    });
  });

  describe('deleteFile', () => {
    it('should delete a file', (done) => {
      minioClient.removeObject = jest.fn().mockResolvedValue(undefined);

      service.deleteFile('test.pdf').subscribe(() => {
        expect(minioClient.removeObject).toHaveBeenCalledWith(
          process.env.MINIO_BUCKET,
          'test.pdf',
        );
        done();
      });
    });

    it('should throw an error if delete fails', (done) => {
      minioClient.removeObject = jest
        .fn()
        .mockRejectedValue(new Error('Delete failed'));

      service.deleteFile('test.pdf').subscribe({
        error: (error) => {
          expect(error.message).toBe('Delete failed');
          done();
        },
      });
    });
  });
});

import { Injectable } from '@nestjs/common';
import * as Minio from 'minio';
import { from, map, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IStorageService } from '../../../common/interfaces/is-storage-service';

@Injectable()
export class MinioService implements IStorageService {
  private minioClient: Minio.Client;

  constructor() {
    this.minioClient = new Minio.Client({
      endPoint: process.env.MINIO_ENDPOINT,
      port: parseInt(process.env.MINIO_PORT),
      useSSL: process.env.MINIO_USESSL === 'true',
      accessKey: process.env.MINIO_ACCESSKEY,
      secretKey: process.env.MINIO_SECRETKEY,
    });
    this.ensureBucket().subscribe();
  }

  ensureBucket(): Observable<boolean | void> {
    return from(this.minioClient.bucketExists(process.env.MINIO_BUCKET)).pipe(
      catchError((error) => {
        if (error.code === 'NoSuchBucket') {
          return from(
            this.minioClient.makeBucket(process.env.MINIO_BUCKET, 'us-east-1'),
          );
        }
        throw error;
      }),
    );
  }

  upload(file: Express.Multer.File): Observable<{ url: string }> {
    const metaData = { 'Content-Type': 'application/pdf' };
    return from(
      this.minioClient.putObject(
        process.env.MINIO_BUCKET,
        file.originalname,
        file.buffer,
        file.size,
        metaData,
      ),
    ).pipe(
      catchError((error) => {
        throw error;
      }),
      map(() => ({
        url: `http://localhost:9000/${process.env.MINIO_BUCKET}/${file.originalname}`,
      })),
    );
  }

  deleteFile(fileName: string): Observable<void> {
    return from(
      this.minioClient.removeObject(process.env.MINIO_BUCKET, fileName),
    ).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }
}

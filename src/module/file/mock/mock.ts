import { of } from 'rxjs';
import { mockTask } from '../../comment/mock/mock';

const file: Express.Multer.File = {
  originalname: 'file.pdf',
  mimetype: 'text/pdf',
  path: 'something',
  buffer: Buffer.from('one,two,three'),
  filename: 'file',
  fieldname: 'file',
  size: 1,
  stream: undefined,
  destination: 'file',
  encoding: 'file',
};

const mockFile = {
  id: '1',
  createdAt: new Date(),
  filename: 'test.txt',
  path: 'https://example.com/test.txt',
  taskId: '1',
  buffers: [],
};

const taskId = '1';

const mockFileService = {
  uploadFile: jest.fn().mockReturnValue(of(mockFile)),
  getBufferByFileId: jest.fn().mockReturnValue(of(mockFile)),
  findAll: jest.fn().mockReturnValue(of([mockFile])),
  findOne: jest.fn().mockReturnValue(of(mockFile)),
  remove: jest.fn().mockReturnValue(of(undefined)),
};

const mockFileRepository = {
  create: jest.fn().mockReturnValue(mockFile),
  save: jest.fn().mockResolvedValue(mockFile),
  findOneBy: jest.fn().mockResolvedValue(mockFile),
  find: jest.fn().mockResolvedValue([mockFile]),
  remove: jest.fn().mockResolvedValue(undefined),
};

const mockTaskRepository = {
  findOneBy: jest.fn().mockResolvedValue(mockTask),
};

const mockStorageService = {
  upload: jest.fn().mockResolvedValue({ url: 'https://example.com/test.txt' }),
  deleteFile: jest.fn().mockResolvedValue(undefined),
};

const mockRedisClient = {
  get: jest.fn().mockResolvedValue(null),
  set: jest.fn().mockResolvedValue(null),
  del: jest.fn().mockResolvedValue(null),
};

const mockCacheService = {
  invalidateCache: jest.fn().mockResolvedValue(null),
};

export {
  taskId,
  file,
  mockFileService,
  mockFile,
  mockCacheService,
  mockFileRepository,
  mockStorageService,
  mockTaskRepository,
  mockRedisClient,
};

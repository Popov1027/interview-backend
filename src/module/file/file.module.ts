import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FileService } from './file.service';
import { FileController } from './file.controller';
import { File } from './entities/file.entity';
import { Task } from '../task/entities/task.entity';
import { MinioService } from './storage/minio.service';
import { S3Service } from './storage/s3.service';
import { CacheService } from '../../core/redis/cache.service';

@Module({
  imports: [TypeOrmModule.forFeature([Task, File])],
  controllers: [FileController],
  providers: [
    FileService,
    CacheService,
    {
      provide: 'S3',
      useClass: S3Service,
    },
    {
      provide: 'MINIO',
      useClass: MinioService,
    },
  ],
})
export class FileModule {}

import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { File } from './entities/file.entity';
import {
  catchError,
  concatMap,
  from,
  mergeMap,
  Observable,
  of,
  take,
} from 'rxjs';
import { map } from 'rxjs/operators';
import { Task } from '../task/entities/task.entity';
import { FileDto } from './dto/file.dto';
import { plainToInstance } from 'class-transformer';
import { IStorageService } from '../../common/interfaces/is-storage-service';
import { BufferDto } from './dto/buffer.dto';
import { RedisClient } from '../../core/redis/redis.client';
import { CacheService } from '../../core/redis/cache.service';

@Injectable()
export class FileService {
  constructor(
    @InjectRepository(Task)
    private readonly taskRepository: Repository<Task>,
    @InjectRepository(File)
    private readonly fileRepository: Repository<File>,
    @Inject('REDIS') private readonly redis: RedisClient,
    @Inject('MINIO') private readonly storageService: IStorageService,
    private readonly cacheManager: CacheService,
  ) {}

  uploadFile(taskId: string, file: Express.Multer.File): Observable<FileDto> {
    return from(this.taskRepository.findOneBy({ id: taskId })).pipe(
      take(1),
      mergeMap((task) => {
        if (!task) {
          throw new HttpException('Task not found', HttpStatus.NOT_FOUND);
        }
        return from(this.storageService.upload(file)).pipe(
          mergeMap((result) => {
            const bufferLines = file.buffer
              .toString('utf8')
              .split('\n' || '\r\n');

            const bufferDtoArray = bufferLines.map((line) => {
              const bufferDto = new BufferDto();
              bufferDto.content = line;
              return bufferDto;
            });

            const newFile = this.fileRepository.create({
              filename: file.originalname,
              buffers: bufferDtoArray,
              path: result.url,
              taskId: taskId,
            });
            return from(this.fileRepository.save(newFile)).pipe(
              map((savedFile) => {
                const fileDto = plainToInstance(FileDto, savedFile);
                from(this.cacheManager.invalidateCache('file_*'));
                return fileDto;
              }),
            );
          }),
          catchError((error) => {
            throw new HttpException(
              `Failed to upload file: ${error}`,
              HttpStatus.INTERNAL_SERVER_ERROR,
            );
          }),
        );
      }),
    );
  }

  getBufferByFileId(fileId: string): Observable<BufferDto[]> {
    return from(this.fileRepository.findOneBy({ id: fileId })).pipe(
      mergeMap((file) => {
        if (!file) {
          throw new HttpException('File not found', HttpStatus.NOT_FOUND);
        }
        return from([file.buffers]).pipe(
          map((buffers) => plainToInstance(BufferDto, buffers)),
        );
      }),
      catchError((error) => {
        throw new HttpException(
          `Failed to get buffer: ${error}`,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }),
    );
  }

  findAll(taskId: string): Observable<FileDto[]> {
    const cacheKey = `file_${taskId}`;
    return from(this.redis.get(cacheKey)).pipe(
      concatMap((cachedValue) => {
        if (cachedValue) {
          return of(cachedValue);
        }
        return from(this.fileRepository.find({ where: { taskId } })).pipe(
          map((files) => {
            const fileDto = plainToInstance(FileDto, files);
            from(this.redis.set(cacheKey, fileDto));
            return fileDto;
          }),
        );
      }),
    );
  }

  findOne(id: string): Observable<FileDto> {
    const cacheKey = `file_${id}`;

    return from(this.redis.get(cacheKey)).pipe(
      concatMap((cachedValue) => {
        if (cachedValue) {
          return of(cachedValue);
        }
        return from(this.fileRepository.findOneBy({ id })).pipe(
          take(1),
          map((file) => {
            if (!file) {
              throw new HttpException(
                `File with ID ${id} not found`,
                HttpStatus.NOT_FOUND,
              );
            }
            const fileDto = plainToInstance(FileDto, file);
            from(this.redis.set(cacheKey, fileDto));
            return fileDto;
          }),
          catchError(() => {
            throw new HttpException(
              `File with ID ${id} not found`,
              HttpStatus.NOT_FOUND,
            );
          }),
        );
      }),
    );
  }

  remove(id: string): Observable<void> {
    return from(this.fileRepository.findOneBy({ id })).pipe(
      take(1),
      mergeMap((file) => {
        if (!file) {
          throw new HttpException('File not found', HttpStatus.NOT_FOUND);
        }
        return from(this.storageService.deleteFile(file.filename)).pipe(
          catchError((error) => {
            throw new HttpException(
              `Failed to delete file from storage: ${error.message}`,
              HttpStatus.INTERNAL_SERVER_ERROR,
            );
          }),
          mergeMap(() =>
            from(this.fileRepository.remove(file)).pipe(
              map(() => {
                from(this.redis.del(`file_${id}`));
                from(this.cacheManager.invalidateCache('file_*'));
                return void 0;
              }),
              catchError((error) => {
                throw new HttpException(
                  `Failed to remove file: ${error.message}`,
                  HttpStatus.INTERNAL_SERVER_ERROR,
                );
              }),
            ),
          ),
        );
      }),
      map(() => {
        return void 0;
      }),
    );
  }
}

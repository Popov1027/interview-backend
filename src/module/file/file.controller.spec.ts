import { Test, TestingModule } from '@nestjs/testing';
import { FileController } from './file.controller';
import { FileService } from './file.service';
import { CacheModule } from '@nestjs/cache-manager';
import { throwError } from 'rxjs';
import { file, mockFile, mockFileService, taskId } from './mock/mock';

describe('FileController', () => {
  let controller: FileController;
  let service: FileService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CacheModule.register()],
      controllers: [FileController],
      providers: [
        {
          provide: FileService,
          useValue: mockFileService,
        },
      ],
    }).compile();

    controller = module.get<FileController>(FileController);
    service = module.get<FileService>(FileService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('uploadFile', () => {
    it('should upload a file', (done) => {
      controller.uploadFile(taskId, file).subscribe((result) => {
        expect(result).toEqual(mockFile);
        expect(service.uploadFile).toHaveBeenCalledWith(taskId, file);
        done();
      });
    });

    it('should handle error when upload a file', (done) => {
      jest
        .spyOn(service, 'uploadFile')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to upload a file')),
        );
      controller.uploadFile(taskId, file).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to upload a file');
          done();
        },
      });
    });
  });

  describe('getBufferByFileId', () => {
    it('should get buffer by file Id', (done) => {
      controller.getBufferByFileId('1').subscribe((result) => {
        expect(result).toEqual(mockFile);
        expect(service.getBufferByFileId).toHaveBeenCalledWith('1');
        done();
      });
    });

    it('should handle error when get buffer by file Id ', (done) => {
      jest
        .spyOn(service, 'getBufferByFileId')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to get buffer by file Id')),
        );
      controller.getBufferByFileId('1').subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to get buffer by file Id');
          done();
        },
      });
    });
  });

  describe('findAll', () => {
    it('should find all files', (done) => {
      controller.findAll(taskId).subscribe((result) => {
        expect(result).toEqual([mockFile]);
        expect(service.findAll).toHaveBeenCalledWith(taskId);
        done();
      });
    });

    it('should handle error when finding all files', (done) => {
      jest
        .spyOn(service, 'findAll')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to get all files')),
        );

      controller.findAll(taskId).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to get all files');
          done();
        },
      });
    });
  });

  describe('findOne', () => {
    it('should find one file by id', (done) => {
      controller.findOne('1').subscribe((result) => {
        expect(result).toEqual(mockFile);
        expect(service.findOne).toHaveBeenCalledWith('1');
        done();
      });
    });

    it('should handle error when finding file by id', (done) => {
      jest
        .spyOn(service, 'findOne')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to get file by id')),
        );
      controller.findOne('1').subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to get file by id');
          done();
        },
      });
    });
  });

  describe('remove', () => {
    it('should remove a file', (done) => {
      controller.remove('1').subscribe((result) => {
        expect(result).toBeUndefined();
        expect(service.remove).toHaveBeenCalledWith('1');
        done();
      });
    });

    it('should handle error when removing file', (done) => {
      jest
        .spyOn(service, 'remove')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to delete file')),
        );
      controller.remove('1').subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to delete file');
          done();
        },
      });
    });
  });
});

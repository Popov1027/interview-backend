import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { HttpStatus } from '@nestjs/common';
import { FileService } from './file.service';
import { File } from './entities/file.entity';
import { Task } from '../task/entities/task.entity';
import { CacheService } from '../../core/redis/cache.service';
import {
  file,
  mockCacheService,
  mockFile,
  mockFileRepository,
  mockRedisClient,
  mockStorageService,
  mockTaskRepository,
} from './mock/mock';

describe('FileService', () => {
  let service: FileService;
  let fileRepository: Repository<File>;
  let taskRepository: Repository<Task>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FileService,
        {
          provide: getRepositoryToken(File),
          useValue: mockFileRepository,
        },
        {
          provide: getRepositoryToken(Task),
          useValue: mockTaskRepository,
        },
        { provide: 'REDIS', useValue: mockRedisClient },
        { provide: 'MINIO', useValue: mockStorageService },
        { provide: CacheService, useValue: mockCacheService },
      ],
    }).compile();

    service = module.get<FileService>(FileService);
    fileRepository = module.get<Repository<File>>(getRepositoryToken(File));
    taskRepository = module.get<Repository<Task>>(getRepositoryToken(Task));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('uploadFile', () => {
    it('should upload a file', (done) => {
      service.uploadFile('1', file).subscribe((result) => {
        expect(result).toEqual(mockFile);
        expect(mockCacheService.invalidateCache).toHaveBeenCalledWith('file_*');
        done();
      });
    });

    it('should throw an error if task not found', (done) => {
      jest.spyOn(taskRepository, 'findOneBy').mockResolvedValueOnce(null);
      service.uploadFile('1', file).subscribe({
        error: (error) => {
          expect(error.response).toBe('Task not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if file upload fails', (done) => {
      jest
        .spyOn(mockStorageService, 'upload')
        .mockRejectedValueOnce(new Error('Upload error'));
      service.uploadFile('1', file).subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to upload file: Error: Upload error',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });

  describe('getBufferByFileId', () => {
    it('should get buffers by file id', (done) => {
      service.getBufferByFileId('1').subscribe((result) => {
        expect(result).toEqual(mockFile.buffers);
        done();
      });
    });

    it('should throw an error if file not found', (done) => {
      jest.spyOn(fileRepository, 'findOneBy').mockResolvedValueOnce(null);
      service.getBufferByFileId('1').subscribe({
        next: () => {
          done.fail('Expected method to throw an error');
        },
        error: (error) => {
          expect(error.response).toBe(
            'Failed to get buffer: HttpException: File not found',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });

    it('should throw an error if failed to get buffer', (done) => {
      jest
        .spyOn(fileRepository, 'findOneBy')
        .mockRejectedValueOnce(new Error('File not found'));
      service.getBufferByFileId('1').subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to get buffer: Error: File not found',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });

  describe('findAll', () => {
    it('should find all files by task id', (done) => {
      service.findAll('1').subscribe((result) => {
        expect(result).toEqual([mockFile]);
        done();
      });
    });

    it('should return cached files', (done) => {
      jest.spyOn(mockRedisClient, 'get').mockResolvedValueOnce([mockFile]);
      service.findAll('1').subscribe((result) => {
        expect(result).toEqual([mockFile]);
        done();
      });
    });
  });

  describe('findOne', () => {
    it('should find one file by id', (done) => {
      service.findOne('1').subscribe((result) => {
        expect(result).toEqual(mockFile);
        done();
      });
    });

    it('should return cached file', (done) => {
      jest.spyOn(mockRedisClient, 'get').mockResolvedValueOnce(mockFile);
      service.findOne('1').subscribe((result) => {
        expect(result).toEqual(mockFile);
        done();
      });
    });

    it('should throw an error if file not found', (done) => {
      jest.spyOn(fileRepository, 'findOneBy').mockResolvedValueOnce(null);
      service.findOne('1').subscribe({
        error: (error) => {
          expect(error.response).toBe('File with ID 1 not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });
  });

  describe('remove', () => {
    it('should remove a file', (done) => {
      service.remove('1').subscribe((result) => {
        expect(result).toBeUndefined();
        expect(mockRedisClient.del).toHaveBeenCalledWith('file_1');
        expect(mockCacheService.invalidateCache).toHaveBeenCalledWith('file_*');
        done();
      });
    });

    it('should throw an error if file not found', (done) => {
      jest.spyOn(fileRepository, 'findOneBy').mockResolvedValueOnce(null);
      service.remove('1').subscribe({
        error: (error) => {
          expect(error.response).toBe('File not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if failed to delete file from storage', (done) => {
      jest.spyOn(fileRepository, 'findOneBy').mockResolvedValueOnce(mockFile);
      jest
        .spyOn(mockStorageService, 'deleteFile')
        .mockRejectedValueOnce(new Error('Delete error'));
      service.remove('1').subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to delete file from storage: Delete error',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });

    it('should throw an error if failed to remove file', (done) => {
      jest.spyOn(fileRepository, 'findOneBy').mockResolvedValueOnce(mockFile);
      jest
        .spyOn(fileRepository, 'remove')
        .mockRejectedValueOnce(new Error('Remove error'));
      service.remove('1').subscribe({
        error: (error) => {
          expect(error.response).toBe('Failed to remove file: Remove error');
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });
});

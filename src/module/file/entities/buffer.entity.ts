import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  Index,
  OneToMany,
} from 'typeorm';
import { File } from './file.entity';
import { Comment } from '../../comment/entities/comment.entity';

@Entity()
export class Buffer {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  content: string;

  @Column({ name: 'file_id' })
  @Index()
  fileId: string;

  @ManyToOne(() => File, (file) => file.buffers, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'file_id' })
  file: File;

  @OneToMany(() => Comment, (comment) => comment.buffer)
  comments!: Comment[];
}

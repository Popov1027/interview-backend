import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  Index,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  OneToMany,
} from 'typeorm';
import { Task } from '../../task/entities/task.entity';
import { Buffer } from './buffer.entity';

@Entity()
export class File {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @Column()
  filename: string;

  @Column()
  path: string;

  @Column({ name: 'task_id' })
  @Index()
  taskId!: string;

  @ManyToOne(() => Task, (task) => task.id, {
    onDelete: 'CASCADE',
    eager: true,
  })
  @JoinColumn({ name: 'task_id' })
  task?: Task;

  @OneToMany(() => Buffer, (buffer) => buffer.file, {
    cascade: true,
    eager: true,
  })
  buffers: Buffer[];
}

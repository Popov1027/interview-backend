import {
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileService } from './file.service';
import {
  ApiTags,
  ApiBearerAuth,
  ApiResponse,
  ApiOperation,
  ApiConsumes,
  ApiBody,
} from '@nestjs/swagger';
import { FileDto } from './dto/file.dto';
import { ProjectDto } from '../project/dto/project.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { from, Observable } from 'rxjs';
import { BufferDto } from './dto/buffer.dto';
import { JwtAuthGuard } from '../../core/auth/guards/auth.guard';

@ApiTags('File')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('file')
export class FileController {
  constructor(private readonly fileService: FileService) {}

  @ApiOperation({ summary: 'Endpoint to upload pdf file' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Upload file',
    type: ProjectDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to upload file',
  })
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'File upload',
    required: true,
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @Post('upload/:taskId')
  uploadFile(
    @Param('taskId') taskId: string,
    @UploadedFile() file: Express.Multer.File,
  ): Observable<FileDto> {
    return from(this.fileService.uploadFile(taskId, file));
  }

  @ApiOperation({ summary: 'Endpoint to get buffer by file' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Get buffer',
    type: FileDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to get buffer',
  })
  @Get('/buffer/:fileId')
  getBufferByFileId(@Query('fileId') fileId: string): Observable<BufferDto[]> {
    return from(this.fileService.getBufferByFileId(fileId));
  }

  @ApiOperation({ summary: 'Endpoint to get all files by task ID' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Get files',
    type: [FileDto],
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to get files',
  })
  @Get()
  findAll(@Query('taskId') taskId: string): Observable<FileDto[]> {
    return from(this.fileService.findAll(taskId));
  }

  @ApiOperation({ summary: 'Endpoint to get file by id' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Get file by id',
    type: FileDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to get file by id',
  })
  @Get(':id')
  findOne(@Param('id') id: string): Observable<FileDto> {
    return from(this.fileService.findOne(id));
  }

  @ApiOperation({ summary: 'Endpoint to delete file' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Delete file',
    type: FileDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to delete file',
  })
  @Delete(':id')
  remove(@Param('id') id: string): Observable<void> {
    return from(this.fileService.remove(id));
  }
}

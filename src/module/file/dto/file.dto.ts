import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import {
  IsArray,
  IsDate,
  IsNotEmpty,
  IsObject,
  IsOptional,
  IsPositive,
  IsString,
} from 'class-validator';
import { Task } from '../../task/entities/task.entity';
import { BufferDto } from './buffer.dto';

@Exclude()
export class FileDto {
  @ApiProperty({
    example: 'e13f257e-e37e-423b-a0a3-81cba53deec8',
    type: String,
  })
  @IsString()
  @IsPositive()
  @Expose()
  id: string;

  @ApiProperty({
    example: '2022-01-22T13:55:37.839Z',
    type: Date,
  })
  @IsDate()
  @IsNotEmpty()
  @Expose()
  createdAt: Date;

  @ApiPropertyOptional({
    example: 'test',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  filename: string;

  @ApiPropertyOptional({
    example: 'path',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  path: string;

  @IsArray()
  @Type(() => BufferDto)
  @Expose()
  buffers: BufferDto[];

  @ApiPropertyOptional({
    type: () => Task,
  })
  @Type(() => Task)
  @IsObject()
  @IsOptional()
  @Expose()
  task: Task;

  @ApiPropertyOptional({
    example: 'e13f257e-e37e-423b-a0a3-81cba53deec8',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  taskId!: string;
}

import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';

@Exclude()
export class BufferDto {
  @ApiProperty({
    example: 'dasdsadfgfdb34gffdfsdcfsfgewadsad',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  id: string;

  @ApiProperty({
    example: 'function fibonacci(num) {',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  content: string;
}

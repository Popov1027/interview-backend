import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsInt,
  IsNotEmpty,
  IsPositive,
  IsString,
} from 'class-validator';
import { Expose } from 'class-transformer';

export class UserDto {
  @ApiProperty({
    example: 'e13f257e-e37e-423b-a0a3-81cba53deec8',
    type: String,
  })
  @IsInt()
  @IsPositive()
  @Expose()
  id: string;

  @ApiProperty({
    example: 'serghei@email.com',
    type: String,
  })
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty({
    example: 'Popov',
    type: String,
  })
  @IsEmail()
  @IsNotEmpty()
  firstName: string;

  @ApiProperty({
    example: 'Serghei',
    type: String,
  })
  @IsEmail()
  @IsNotEmpty()
  lastName: string;

  @ApiProperty({
    example: '+37367528138',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  phoneNumber: string;
}

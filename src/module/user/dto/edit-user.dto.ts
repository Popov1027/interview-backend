import { PartialType } from '@nestjs/mapped-types';
import { RegisterDto } from '../../../core/auth/dto/register.dto';

export class EditUserDto extends PartialType(RegisterDto) {}

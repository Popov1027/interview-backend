import { Test, TestingModule } from '@nestjs/testing';
import { HttpException, HttpStatus } from '@nestjs/common';
import { UserService } from './user.service';
import { Repository } from 'typeorm';
import { UserDecorator } from '../../core/auth/interface/token-payload.interface';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UserEntity } from './entities/user.entity';

const mockAuthRepository = {
  findOneBy: jest.fn(),
  create: jest.fn(),
  save: jest.fn(),
  merge: jest.fn(),
};

const mockRedisClient = {
  get: jest.fn().mockResolvedValue(null),
  set: jest.fn().mockResolvedValue(null),
};

const mockUserEntity = {
  id: '1',
  email: 'test@test.com',
  password: '$2b$10$examplehash',
};

describe('UserService', () => {
  let service: UserService;
  let repository: Repository<UserEntity>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getRepositoryToken(UserEntity),
          useValue: mockAuthRepository,
        },
        { provide: 'REDIS', useValue: mockRedisClient },
      ],
    }).compile();

    service = module.get<UserService>(UserService);
    repository = module.get<Repository<UserEntity>>(
      getRepositoryToken(UserEntity),
    );
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('profile', () => {
    const mockUserDecorator: UserDecorator = {
      userId: '1',
      email: 'test@test.com',
    };

    it('should get user profile', (done) => {
      mockRedisClient.get.mockResolvedValueOnce(null);
      mockAuthRepository.findOneBy.mockResolvedValueOnce(mockUserEntity);

      service.profile(mockUserDecorator).subscribe((result) => {
        expect(result).toEqual(mockUserEntity);
        expect(mockRedisClient.get).toHaveBeenCalledWith('profile');
        expect(mockAuthRepository.findOneBy).toHaveBeenCalledWith({
          id: mockUserDecorator.userId,
        });
        done();
      });
    });

    it('should throw an error if user profile is not found', (done) => {
      mockRedisClient.get.mockResolvedValueOnce(null);
      mockAuthRepository.findOneBy.mockResolvedValueOnce(null);

      service.profile(mockUserDecorator).subscribe({
        error: (error) => {
          expect(error).toBeInstanceOf(HttpException);
          expect(error.message).toBe(
            `User profile with ID ${mockUserDecorator.userId} not found`,
          );
          expect(error.getStatus()).toBe(HttpStatus.NOT_FOUND);
          expect(mockRedisClient.get).toHaveBeenCalledWith('profile');
          expect(mockAuthRepository.findOneBy).toHaveBeenCalledWith({
            id: mockUserDecorator.userId,
          });
          done();
        },
      });
    });

    it('should get user profile from cache', (done) => {
      mockRedisClient.get.mockResolvedValueOnce(mockUserEntity);

      service.profile(mockUserDecorator).subscribe((result) => {
        expect(result).toEqual(mockUserEntity);
        expect(mockRedisClient.get).toHaveBeenCalledWith('profile');
        expect(mockAuthRepository.findOneBy).not.toHaveBeenCalled();
        done();
      });
    });
  });

  describe('editProfile', () => {
    const mockUserDecorator: UserDecorator = {
      userId: '1',
      email: 'test@test.com',
    };
    const editProfileDto = { firstName: 'John' };

    it('should edit user profile', (done) => {
      mockAuthRepository.findOneBy.mockResolvedValueOnce(mockUserEntity);
      mockAuthRepository.save.mockResolvedValueOnce(mockUserEntity);

      service
        .editProfile(mockUserDecorator, editProfileDto)
        .subscribe((result) => {
          expect(result.firstName).toBe(editProfileDto.firstName);
          expect(mockAuthRepository.findOneBy).toHaveBeenCalledWith({
            id: mockUserDecorator.userId,
          });
          expect(mockAuthRepository.save).toHaveBeenCalledWith(mockUserEntity);
          done();
        });
    });

    it('should throw an error if user is not found', (done) => {
      mockAuthRepository.findOneBy.mockResolvedValueOnce(null);

      service.editProfile(mockUserDecorator, editProfileDto).subscribe({
        error: (error) => {
          expect(error).toBeInstanceOf(HttpException);
          expect(error.message).toBe('User not found');
          expect(error.getStatus()).toBe(HttpStatus.NOT_FOUND);
          expect(mockAuthRepository.findOneBy).toHaveBeenCalledWith({
            id: mockUserDecorator.userId,
          });
          done();
        },
      });
    });

    it('should throw an error if failed to edit profile', (done) => {
      mockAuthRepository.findOneBy.mockResolvedValueOnce(mockUserEntity);
      mockAuthRepository.save.mockRejectedValueOnce(
        new Error('Database error'),
      );

      service.editProfile(mockUserDecorator, editProfileDto).subscribe({
        error: (error) => {
          expect(error).toBeInstanceOf(HttpException);
          expect(error.message).toBe(
            'Failed to edit user: Error: Database error',
          );
          expect(error.getStatus()).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          expect(mockAuthRepository.findOneBy).toHaveBeenCalledWith({
            id: mockUserDecorator.userId,
          });
          expect(mockAuthRepository.save).toHaveBeenCalledWith(mockUserEntity);
          done();
        },
      });
    });
  });
});

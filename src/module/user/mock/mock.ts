import { UserDecorator } from '../../../core/auth/interface/token-payload.interface';
import { LoginDto } from '../../../core/auth/dto/login.dto';
import { RegisterDto } from '../../../core/auth/dto/register.dto';
import { TokenDto } from '../../../core/auth/dto/token.dto';
import { of } from 'rxjs';
import { UserEntity } from '../entities/user.entity';

const userDto = {
  id: 'aaeb9a0d-9079-4bb8-a771-6cb47d53def5',
  email: 'serghei@email.com',
  firstName: 'Serghei',
  lastName: 'Popov',
  phoneNumber: '+37364528116',
};

const user: UserDecorator = {
  userId: '1',
  email: 'test@mail.com',
};

const loginDto: LoginDto = {
  email: 'test@test.com',
  password: 'password',
};

const registerDto: RegisterDto = {
  email: 'test@test.com',
  password: 'password',
  firstName: 'First name',
  lastName: 'Last name',
  phoneNumber: '+37367528138',
};

const editUserDto = { ...registerDto };

const tokenDto: TokenDto = {
  accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9',
  refreshToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9',
};

const mockAuth: UserEntity = {
  id: '1',
  email: 'test@mail.com',
  firstName: 'First name',
  lastName: 'Last name',
  phoneNumber: '+37367528138',
  tasks: [],
  comments: [],
  password: 'Test123!',
};

const mockAuthService = {
  register: jest.fn().mockReturnValue(of(tokenDto)),
  login: jest.fn().mockReturnValue(of(tokenDto)),
  profile: jest.fn().mockReturnValue(of(mockAuth)),
  editProfile: jest.fn().mockReturnValue(of(userDto)),
};

export {
  user,
  userDto,
  loginDto,
  registerDto,
  editUserDto,
  tokenDto,
  mockAuth,
  mockAuthService,
};

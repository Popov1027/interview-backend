import {
  Controller,
  Body,
  HttpStatus,
  HttpCode,
  UseGuards,
  Get,
  Patch,
} from '@nestjs/common';
import { UserService } from './user.service';
import { from, Observable } from 'rxjs';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { User } from './dto/user.decorator';
import { UserDecorator } from '../../core/auth/interface/token-payload.interface';
import { JwtAuthGuard } from '../../core/auth/guards/auth.guard';
import { UserDto } from './dto/user.dto';
import { EditUserDto } from './dto/edit-user.dto';
import { UserEntity } from './entities/user.entity';

@ApiTags('User')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({
    summary: 'User profile',
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Get user profile',
    type: UserDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to get user profile',
  })
  @HttpCode(HttpStatus.CREATED)
  @Get('profile')
  profile(@User() user: UserDecorator): Observable<UserEntity> {
    return from(this.userService.profile(user));
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({
    summary: 'Edit user profile',
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Edit user profile',
    type: UserDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to edit user profile',
  })
  @HttpCode(HttpStatus.CREATED)
  @Patch('profile')
  editProfile(
    @User() user: UserDecorator,
    @Body() editProfileDto: EditUserDto,
  ): Observable<EditUserDto> {
    return from(this.userService.editProfile(user, editProfileDto));
  }
}

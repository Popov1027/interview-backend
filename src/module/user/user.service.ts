import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';

import * as bcrypt from 'bcrypt';
import {
  Observable,
  from,
  map,
  catchError,
  take,
  mergeMap,
  of,
  concatMap,
} from 'rxjs';
import { UserDecorator } from '../../core/auth/interface/token-payload.interface';
import { plainToInstance } from 'class-transformer';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EditUserDto } from './dto/edit-user.dto';
import { RedisClient } from '../../core/redis/redis.client';
import { UserEntity } from './entities/user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly authRepository: Repository<UserEntity>,
    @Inject('REDIS') private readonly redis: RedisClient,
  ) {}

  profile(user: UserDecorator): Observable<UserEntity> {
    const cacheKey = 'profile';
    return from(this.redis.get(cacheKey)).pipe(
      concatMap((cachedProfile) => {
        if (cachedProfile) {
          return of(cachedProfile);
        }
        return from(this.authRepository.findOneBy({ id: user.userId })).pipe(
          take(1),
          map((userProfile) => {
            const userDto = plainToInstance(UserEntity, userProfile);
            from(this.redis.set(cacheKey, userDto));
            return userDto;
          }),
          catchError(() => {
            throw new HttpException(
              `User profile with ID ${user.userId} not found`,
              HttpStatus.NOT_FOUND,
            );
          }),
        );
      }),
    );
  }

  editProfile(
    user: UserDecorator,
    editProfileDto: EditUserDto,
  ): Observable<UserEntity> {
    const cacheKey = 'profile';
    return from(this.authRepository.findOneBy({ id: user.userId })).pipe(
      take(1),
      mergeMap((user) => {
        if (!user) {
          throw new HttpException('User not found', HttpStatus.NOT_FOUND);
        }

        if (!editProfileDto.password) {
          throw new HttpException(
            'Password is required',
            HttpStatus.BAD_REQUEST,
          );
        }

        return from(bcrypt.hash(editProfileDto.password, 10)).pipe(
          concatMap((hashedPassword) => {
            const newUser: EditUserDto = {
              ...editProfileDto,
              password: hashedPassword,
            };

            this.authRepository.merge(user, newUser);
            return from(this.authRepository.save(user)).pipe(
              map((user) => {
                const editUserDto = plainToInstance(UserEntity, user);
                from(this.redis.set(cacheKey, editUserDto));
                return editUserDto;
              }),
              catchError((error) => {
                throw new HttpException(
                  `Failed to edit user: ${error}`,
                  HttpStatus.INTERNAL_SERVER_ERROR,
                );
              }),
            );
          }),
        );
      }),
    );
  }
}

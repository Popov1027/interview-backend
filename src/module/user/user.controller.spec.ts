import { UserController } from './user.controller';
import { UserService } from './user.service';
import { Test, TestingModule } from '@nestjs/testing';
import { CacheModule } from '@nestjs/cache-manager';
import { throwError } from 'rxjs';
import {
  editUserDto,
  mockAuth,
  mockAuthService,
  user,
  userDto,
} from './mock/mock';

describe('UserController', () => {
  let controller: UserController;
  let service: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CacheModule.register()],
      controllers: [UserController],
      providers: [
        {
          provide: UserService,
          useValue: mockAuthService,
        },
      ],
    }).compile();

    controller = module.get<UserController>(UserController);
    service = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('profile', () => {
    it('should find user profile', (done) => {
      controller.profile(user).subscribe((result) => {
        expect(result).toEqual(mockAuth);
        expect(service.profile).toHaveBeenCalledWith(user);
        done();
      });
    });

    it('should handle error when finding subactivity by id', (done) => {
      jest
        .spyOn(service, 'profile')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to get subactivity by id')),
        );
      controller.profile(user).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to get subactivity by id');
          done();
        },
      });
    });
  });

  describe('editProfile', () => {
    it('should update a subactivity', (done) => {
      controller.editProfile(user, editUserDto).subscribe((result) => {
        expect(result).toEqual(userDto);
        expect(service.editProfile).toHaveBeenCalledWith(user, editUserDto);
        done();
      });
    });

    it('should handle error when updating subactivity', (done) => {
      jest
        .spyOn(service, 'editProfile')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to update subactivity')),
        );
      controller.editProfile(user, editUserDto).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to update subactivity');
          done();
        },
      });
    });
  });
});

import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  HttpStatus,
  Query,
} from '@nestjs/common';
import { SubactivityService } from './subactivity.service';
import { CreateSubactivityDto } from './dto/create-subactivity.dto';
import { UpdateSubactivityDto } from './dto/update-subactivity.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { SubactivityDto } from './dto/subactivity.dto';
import { from, Observable } from 'rxjs';
import { SubactivityQueryDto } from './dto/subactivity.query.dto';
import { JwtAuthGuard } from '../../core/auth/guards/auth.guard';

@ApiTags('Subactivity')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('subactivity')
export class SubactivityController {
  constructor(private readonly subactivityService: SubactivityService) {}

  @ApiOperation({ summary: 'Endpoint to create subactivity' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Subactivity was created',
    type: SubactivityDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to create subactivity',
  })
  @Post()
  create(
    @Body() createSubactivityDto: CreateSubactivityDto,
  ): Observable<SubactivityDto> {
    return from(this.subactivityService.create(createSubactivityDto));
  }

  @ApiOperation({ summary: 'Endpoint to get all subactivities' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Get all subactivities',
    type: [SubactivityDto],
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to get all subactivities',
  })
  @Get()
  findAll(
    @Query() subactivityQueryDto: SubactivityQueryDto,
  ): Observable<SubactivityDto[]> {
    return from(this.subactivityService.findAll(subactivityQueryDto));
  }

  @ApiOperation({ summary: 'Endpoint to get subactivity by id' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Get subactivity by id',
    type: SubactivityDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to get subactivity by id',
  })
  @Get(':id')
  findOne(@Param('id') id: string): Observable<SubactivityDto> {
    return from(this.subactivityService.findOne(id));
  }

  @ApiOperation({ summary: 'Endpoint to update subactivity' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Update subactivity',
    type: SubactivityDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to update subactivity',
  })
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateSubactivityDto: UpdateSubactivityDto,
  ): Observable<SubactivityDto> {
    return from(this.subactivityService.update(id, updateSubactivityDto));
  }

  @ApiOperation({ summary: 'Endpoint to delete subactivity' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Delete subactivity',
    type: SubactivityDto,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Failed to delete subactivity',
  })
  @Delete(':id')
  remove(@Param('id') id: string): Observable<void> {
    return from(this.subactivityService.remove(id));
  }
}

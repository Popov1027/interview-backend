import { Test, TestingModule } from '@nestjs/testing';
import { SubactivityController } from './subactivity.controller';
import { SubactivityService } from './subactivity.service';
import { throwError } from 'rxjs';
import {
  createSubactivityDto,
  mockSubactivity,
  mockSubactivityService,
  queryDto,
  updateSubactivityDto,
} from './mock/mock';

describe('SubactivityController', () => {
  let controller: SubactivityController;
  let service: SubactivityService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SubactivityController],
      providers: [
        {
          provide: SubactivityService,
          useValue: mockSubactivityService,
        },
      ],
    }).compile();

    controller = module.get<SubactivityController>(SubactivityController);
    service = module.get<SubactivityService>(SubactivityService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('create', () => {
    it('should create a subactivity', (done) => {
      controller.create(createSubactivityDto).subscribe((result) => {
        expect(result).toEqual(mockSubactivity);
        expect(service.create).toHaveBeenCalledWith(createSubactivityDto);
        done();
      });
    });

    it('should handle error when creating subactivity', (done) => {
      jest
        .spyOn(service, 'create')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to create subactivity')),
        );
      controller.create(createSubactivityDto).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to create subactivity');
          done();
        },
      });
    });
  });

  describe('findAll', () => {
    it('should find all subactivities', (done) => {
      controller.findAll(queryDto).subscribe((result) => {
        expect(result).toEqual([mockSubactivity]);
        expect(service.findAll).toHaveBeenCalledWith(queryDto);
        done();
      });
    });

    it('should handle error when finding all subactivities', (done) => {
      jest
        .spyOn(service, 'findAll')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to get all subactivities')),
        );

      controller.findAll(queryDto).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to get all subactivities');
          done();
        },
      });
    });
  });

  describe('findOne', () => {
    it('should find one subactivity by id', (done) => {
      controller.findOne('1').subscribe((result) => {
        expect(result).toEqual(mockSubactivity);
        expect(service.findOne).toHaveBeenCalledWith('1');
        done();
      });
    });

    it('should handle error when finding subactivity by id', (done) => {
      jest
        .spyOn(service, 'findOne')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to get subactivity by id')),
        );
      controller.findOne('1').subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to get subactivity by id');
          done();
        },
      });
    });
  });

  describe('update', () => {
    it('should update a subactivity', (done) => {
      controller.update('1', updateSubactivityDto).subscribe((result) => {
        expect(result).toEqual(mockSubactivity);
        expect(service.update).toHaveBeenCalledWith('1', updateSubactivityDto);
        done();
      });
    });

    it('should handle error when updating subactivity', (done) => {
      jest
        .spyOn(service, 'update')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to update subactivity')),
        );
      controller.update('1', updateSubactivityDto).subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to update subactivity');
          done();
        },
      });
    });
  });

  describe('remove', () => {
    it('should remove a subactivity', (done) => {
      controller.remove('1').subscribe((result) => {
        expect(result).toBeUndefined();
        expect(service.remove).toHaveBeenCalledWith('1');
        done();
      });
    });

    it('should handle error when removing subactivity', (done) => {
      jest
        .spyOn(service, 'remove')
        .mockReturnValueOnce(
          throwError(() => new Error('Failed to delete subactivity')),
        );
      controller.remove('1').subscribe({
        error: (error) => {
          expect(error.message).toBe('Failed to delete subactivity');
          done();
        },
      });
    });
  });
});

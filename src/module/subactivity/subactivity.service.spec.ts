import { Test, TestingModule } from '@nestjs/testing';
import { SubactivityService } from './subactivity.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Subactivity } from './entities/subactivity.entity';
import { CacheService } from '../../core/redis/cache.service';
import { Repository } from 'typeorm';
import { HttpStatus } from '@nestjs/common';
import {
  createSubactivityDto,
  mockCacheService,
  mockRedisClient,
  mockSubactivity,
  mockSubactivityRepository,
  queryDto,
  updateSubactivityDto,
} from './mock/mock';

describe('SubactivityService', () => {
  let service: SubactivityService;
  let repository: Repository<Subactivity>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SubactivityService,
        {
          provide: getRepositoryToken(Subactivity),
          useValue: mockSubactivityRepository,
        },
        { provide: 'REDIS', useValue: mockRedisClient },
        { provide: CacheService, useValue: mockCacheService },
      ],
    }).compile();

    service = module.get<SubactivityService>(SubactivityService);
    repository = module.get<Repository<Subactivity>>(
      getRepositoryToken(Subactivity),
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('should create a subactivity', (done) => {
      service.create(createSubactivityDto).subscribe((result) => {
        expect(result).toEqual(mockSubactivity);
        expect(mockCacheService.invalidateCache).toHaveBeenCalledWith(
          'subactivity_*',
        );
        done();
      });
    });

    it('should throw an error if creation fails', (done) => {
      jest
        .spyOn(repository, 'save')
        .mockRejectedValueOnce(new Error('Error creating subactivity'));
      service.create({ ...mockSubactivity }).subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to create subactivity: Error: Error creating subactivity',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });

  describe('findAll', () => {
    it('should find all subactivities', (done) => {
      service.findAll(queryDto).subscribe((result) => {
        expect(result).toEqual([mockSubactivity]);
        done();
      });
    });

    it('should return cached subactivities', (done) => {
      jest
        .spyOn(mockRedisClient, 'get')
        .mockResolvedValueOnce([mockSubactivity]);
      service.findAll(queryDto).subscribe((result) => {
        expect(result).toEqual([mockSubactivity]);
        done();
      });
    });
  });

  describe('findOne', () => {
    it('should find one subactivity', (done) => {
      service.findOne('1').subscribe((result) => {
        expect(result).toEqual(mockSubactivity);
        done();
      });
    });

    it('should return cached subactivity', (done) => {
      jest.spyOn(mockRedisClient, 'get').mockResolvedValueOnce(mockSubactivity);
      service.findOne('1').subscribe((result) => {
        expect(result).toEqual(mockSubactivity);
        done();
      });
    });

    it('should throw an error if subactivity not found', (done) => {
      jest.spyOn(repository, 'findOneBy').mockResolvedValueOnce(null);
      service.findOne('1').subscribe({
        error: (error) => {
          expect(error.response).toBe('Subactivity with ID 1 not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });
  });

  describe('update', () => {
    it('should update a subactivity', (done) => {
      service.update('1', updateSubactivityDto).subscribe((result) => {
        expect(result).toEqual(mockSubactivity);
        expect(mockCacheService.invalidateCache).toHaveBeenCalledWith(
          'subactivity_*',
        );
        done();
      });
    });

    it('should throw an error if subactivity not found', (done) => {
      jest.spyOn(repository, 'findOneBy').mockResolvedValueOnce(null);
      service.update('1', updateSubactivityDto).subscribe({
        error: (error) => {
          expect(error.response).toBe('Subactivity not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if failed to update subactivity', (done) => {
      jest
        .spyOn(repository, 'findOneBy')
        .mockResolvedValueOnce(mockSubactivity);
      jest
        .spyOn(repository, 'save')
        .mockRejectedValueOnce(new Error('Error updating subactivity'));

      service.update('1', updateSubactivityDto).subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to update subactivity: Error: Error updating subactivity',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });

  describe('remove', () => {
    it('should remove a subactivity', (done) => {
      service.remove('1').subscribe((result) => {
        expect(result).toBeUndefined();
        expect(mockRedisClient.del).toHaveBeenCalledWith('subactivity_1');
        expect(mockCacheService.invalidateCache).toHaveBeenCalledWith(
          'subactivity_*',
        );
        done();
      });
    });

    it('should throw an error if subactivity not found', (done) => {
      jest.spyOn(repository, 'findOneBy').mockResolvedValueOnce(null);
      service.remove('1').subscribe({
        error: (error) => {
          expect(error.response).toBe('Subactivity not found');
          expect(error.status).toBe(HttpStatus.NOT_FOUND);
          done();
        },
      });
    });

    it('should throw an error if failed to remove subactivity', (done) => {
      jest
        .spyOn(repository, 'findOneBy')
        .mockResolvedValueOnce(mockSubactivity);
      jest
        .spyOn(repository, 'remove')
        .mockRejectedValueOnce(new Error('Error removing subactivity'));
      service.remove('1').subscribe({
        error: (error) => {
          expect(error.response).toBe(
            'Failed to remove subactivity: Error: Error removing subactivity',
          );
          expect(error.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
          done();
        },
      });
    });
  });
});

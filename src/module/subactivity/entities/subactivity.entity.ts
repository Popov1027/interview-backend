import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Index,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Task } from '../../task/entities/task.entity';
import { TaskPriority, TaskStatus } from '../../../common/enums/task.enum';

@Entity()
export class Subactivity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
  })
  updatedAt: Date;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column({ nullable: true })
  deadline: string;

  @Column({
    type: 'enum',
    enum: TaskPriority,
    default: TaskPriority.MEDIUM,
  })
  priority: TaskPriority;

  @Column({
    type: 'enum',
    enum: TaskStatus,
    default: TaskStatus.TODO,
  })
  status: TaskStatus;

  @Column({ name: 'task_id', nullable: false })
  @Index()
  taskId!: string;

  @ManyToOne(() => Task, (task) => task.id, {
    onDelete: 'CASCADE',
    eager: true,
  })
  @JoinColumn({ name: 'task_id' })
  task?: Task;
}

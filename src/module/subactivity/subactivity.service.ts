import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { CreateSubactivityDto } from './dto/create-subactivity.dto';
import { UpdateSubactivityDto } from './dto/update-subactivity.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Subactivity } from './entities/subactivity.entity';
import {
  catchError,
  concatMap,
  from,
  map,
  mergeMap,
  Observable,
  of,
  take,
} from 'rxjs';
import { plainToInstance } from 'class-transformer';
import { SubactivityDto } from './dto/subactivity.dto';
import { SubactivityQueryDto } from './dto/subactivity.query.dto';
import { RedisClient } from '../../core/redis/redis.client';
import { CacheService } from '../../core/redis/cache.service';

@Injectable()
export class SubactivityService {
  constructor(
    @InjectRepository(Subactivity)
    private readonly subactivityRepository: Repository<Subactivity>,
    @Inject('REDIS') private readonly redis: RedisClient,
    private readonly cacheManager: CacheService,
  ) {}

  create(
    createSubactivityDto: CreateSubactivityDto,
  ): Observable<SubactivityDto> {
    const subactivity = this.subactivityRepository.create(createSubactivityDto);
    return from(this.subactivityRepository.save(subactivity)).pipe(
      map((subactivity) => {
        const subactivityDto = plainToInstance(SubactivityDto, subactivity);
        from(this.cacheManager.invalidateCache('subactivity_*'));
        return subactivityDto;
      }),
      catchError((error) => {
        throw new HttpException(
          `Failed to create subactivity: ${error}`,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }),
    );
  }

  findAll({
    priority,
    status,
    sort,
    name,
    taskId,
  }: SubactivityQueryDto): Observable<SubactivityDto[]> {
    const cacheKey = `subactivity_${priority}_${status}_${sort}_${name}_${taskId}`;
    return from(this.redis.get(cacheKey)).pipe(
      concatMap((cachedSubactivity) => {
        if (cachedSubactivity) {
          return of(cachedSubactivity);
        }
        return from(
          this.subactivityRepository.find({
            where: { taskId, name, priority, status },
            order: { ['priority']: sort },
          }),
        ).pipe(
          map((subactivity) => {
            const subactivityDto = plainToInstance(SubactivityDto, subactivity);
            from(this.redis.set(cacheKey, subactivityDto));
            return subactivityDto;
          }),
        );
      }),
    );
  }

  findOne(id: string): Observable<SubactivityDto> {
    const cacheKey = `subactivity_${id}`;
    return from(this.redis.get(cacheKey)).pipe(
      concatMap((cachedSubactivity) => {
        if (cachedSubactivity) {
          return of(cachedSubactivity);
        }
        return from(this.subactivityRepository.findOneBy({ id })).pipe(
          take(1),
          map((subactivity) => {
            if (!subactivity) {
              throw new HttpException(
                `Subactivity with ID ${id} not found`,
                HttpStatus.NOT_FOUND,
              );
            }
            const subactivityDto = plainToInstance(SubactivityDto, subactivity);
            from(this.redis.set(cacheKey, subactivityDto));
            return subactivityDto;
          }),
          catchError(() => {
            throw new HttpException(
              `Subactivity with ID ${id} not found`,
              HttpStatus.NOT_FOUND,
            );
          }),
        );
      }),
    );
  }

  update(
    id: string,
    updateSubactivityDto: UpdateSubactivityDto,
  ): Observable<SubactivityDto> {
    return from(this.subactivityRepository.findOneBy({ id })).pipe(
      take(1),
      mergeMap((subactivity) => {
        if (!subactivity) {
          throw new HttpException(
            'Subactivity not found',
            HttpStatus.NOT_FOUND,
          );
        }

        this.subactivityRepository.merge(subactivity, updateSubactivityDto);
        return from(this.subactivityRepository.save(subactivity)).pipe(
          map((subactivity) => {
            const subactivityDto = plainToInstance(SubactivityDto, subactivity);
            from(this.redis.set(`subactivity_${id}`, subactivityDto));
            from(this.cacheManager.invalidateCache('subactivity_*'));
            return subactivityDto;
          }),
          catchError((error) => {
            throw new HttpException(
              `Failed to update subactivity: ${error}`,
              HttpStatus.INTERNAL_SERVER_ERROR,
            );
          }),
        );
      }),
    );
  }

  remove(id: string): Observable<void> {
    return from(this.subactivityRepository.findOneBy({ id })).pipe(
      mergeMap((subactivity) => {
        if (!subactivity) {
          throw new HttpException(
            `Subactivity not found`,
            HttpStatus.NOT_FOUND,
          );
        }

        return from(this.subactivityRepository.remove(subactivity)).pipe(
          map(() => {
            from(this.redis.del(`subactivity_${id}`));
            from(this.cacheManager.invalidateCache('subactivity_*'));
            return void 0;
          }),
          catchError((error) => {
            throw new HttpException(
              `Failed to remove subactivity: ${error}`,
              HttpStatus.INTERNAL_SERVER_ERROR,
            );
          }),
        );
      }),
      map(() => {
        return void 0;
      }),
    );
  }
}

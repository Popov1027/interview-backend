import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import {
  IsDate,
  IsInt,
  IsNotEmpty,
  IsObject,
  IsOptional,
  IsPositive,
  IsString,
} from 'class-validator';
import { TaskPriority, TaskStatus } from '../../../common/enums/task.enum';
import { Task } from '../../task/entities/task.entity';

@Exclude()
export class SubactivityDto {
  @ApiProperty({
    example: 'e13f257e-e37e-423b-a0a3-81cba53deec8',
    type: String,
  })
  @IsInt()
  @IsPositive()
  @Expose()
  id: string;

  @ApiProperty({
    example: '2022-01-22T13:55:37.839Z',
    type: Date,
  })
  @IsDate()
  @IsNotEmpty()
  @Expose()
  createdAt: Date;

  @ApiProperty({
    example: '2022-01-22T13:55:37.839Z',
    type: Date,
  })
  @IsDate()
  @IsNotEmpty()
  @Expose()
  updatedAt: Date;

  @ApiProperty({
    example: '2022-01-22T13:55:37.839Z',
    type: Date,
  })
  @IsDate()
  @IsNotEmpty()
  @Expose()
  deadline: Date;

  @ApiProperty({
    example: 'Atlanta',
    nullable: false,
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  name!: string;

  @ApiPropertyOptional({
    example:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    nullable: true,
  })
  @IsString()
  @IsOptional()
  @Expose()
  description?: string;

  @ApiProperty({
    example: 'medium',
    type: 'enum',
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  priority: TaskPriority;

  @ApiProperty({
    example: 'todo',
    type: 'enum',
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  status: TaskStatus;

  @ApiPropertyOptional({
    example: 'e13f257e-e37e-423b-a0a3-81cba53deec8',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  taskId!: string;

  @ApiPropertyOptional({
    type: () => Task,
  })
  @Type(() => Task)
  @IsObject()
  @IsOptional()
  @Expose()
  task: Task;
}

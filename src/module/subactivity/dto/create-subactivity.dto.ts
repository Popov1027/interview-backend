import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { TaskPriority, TaskStatus } from '../../../common/enums/task.enum';
import { Expose } from 'class-transformer';
import { IsFutureDate } from '../../../common/decorators/is-future-date.validator';

export class CreateSubactivityDto {
  @ApiProperty({
    example: 'Subactivity',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    example:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  description: string;

  @ApiProperty({
    nullable: false,
    example: '12/05/2025',
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  @IsFutureDate({ message: 'Deadline must be a future date' })
  deadline!: string;

  @ApiProperty({
    example: 'medium',
    type: 'enum',
    enum: TaskPriority,
    default: TaskPriority.MEDIUM,
  })
  @IsString()
  @IsNotEmpty()
  priority: TaskPriority;

  @ApiProperty({
    example: 'todo',
    type: 'enum',
    enum: TaskStatus,
    default: TaskStatus.TODO,
  })
  @IsString()
  @IsNotEmpty()
  status: TaskStatus;

  @ApiProperty({
    example: 'dccaefdd-4c21-4643-abb3-0e322f0d4777',
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  taskId: string;
}

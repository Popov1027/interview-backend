import { ApiProperty, OmitType } from '@nestjs/swagger';
import { TaskQueryDto } from '../../task/dto/task.query.dto';

export class SubactivityQueryDto extends OmitType(TaskQueryDto, ['projectId']) {
  @ApiProperty({
    type: String,
    required: false,
  })
  taskId: string;
}

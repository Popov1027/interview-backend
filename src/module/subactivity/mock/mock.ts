import {
  Sort,
  TaskPriority,
  TaskStatus,
} from '../../../common/enums/task.enum';
import { of } from 'rxjs';

const createSubactivityDto = {
  name: 'Test Subactivity',
  taskId: '1',
  priority: TaskPriority.LOW,
  status: TaskStatus.TODO,
  description: 'Test description',
  deadline: '05/12/2025',
};

const updateSubactivityDto = { ...createSubactivityDto };

const mockSubactivity = {
  id: '1',
  name: 'Test Subactivity',
  taskId: '1',
  priority: TaskPriority.LOW,
  status: TaskStatus.TODO,
  description: 'Test description',
  deadline: '05/12/2025',
  createdAt: new Date(),
  updatedAt: new Date(),
};

const queryDto = {
  priority: TaskPriority.LOW,
  status: TaskStatus.TODO,
  sort: Sort.ASC,
  name: 'Test',
  taskId: '1',
};

const mockSubactivityService = {
  create: jest.fn().mockReturnValue(of(mockSubactivity)),
  findAll: jest.fn().mockReturnValue(of([mockSubactivity])),
  findOne: jest.fn().mockReturnValue(of(mockSubactivity)),
  update: jest.fn().mockReturnValue(of(mockSubactivity)),
  remove: jest.fn().mockReturnValue(of(undefined)),
};

const mockSubactivityRepository = {
  create: jest.fn().mockReturnValue(mockSubactivity),
  save: jest.fn().mockResolvedValue(mockSubactivity),
  findOneBy: jest.fn().mockResolvedValue(mockSubactivity),
  find: jest.fn().mockResolvedValue([mockSubactivity]),
  merge: jest.fn().mockResolvedValue(mockSubactivity),
  remove: jest.fn().mockResolvedValue(undefined),
};

const mockRedisClient = {
  get: jest.fn().mockResolvedValue(null),
  set: jest.fn().mockResolvedValue(null),
  del: jest.fn().mockResolvedValue(null),
};

const mockCacheService = {
  invalidateCache: jest.fn().mockResolvedValue(null),
};

export {
  createSubactivityDto,
  updateSubactivityDto,
  queryDto,
  mockSubactivityService,
  mockSubactivity,
  mockSubactivityRepository,
  mockCacheService,
  mockRedisClient,
};

import { Module } from '@nestjs/common';
import { SubactivityService } from './subactivity.service';
import { SubactivityController } from './subactivity.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Subactivity } from './entities/subactivity.entity';
import { CacheService } from '../../core/redis/cache.service';

@Module({
  imports: [TypeOrmModule.forFeature([Subactivity])],
  controllers: [SubactivityController],
  providers: [SubactivityService, CacheService],
})
export class SubactivityModule {}
